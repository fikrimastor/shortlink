<?php

namespace App\Actions\Fortify;

use App\Models\Team;
use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
        ])->validate();

        return DB::transaction(function () use ($input) {
            return tap(User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
            ]), function (User $user) use ($input) {
                $this->createTeam($user);
                $user->profile()->create([
                    'phone'=>$input['phone'],
                ]);
            });
        });

        // $user = User::create([
        //         'name' => $input['name'],
        //         'email' => $input['email'], 
        //         'password' => Hash::make($input['password']),
        // ]);
        // $user = User::create(['name' => $input['name'], 'email' => $input['email'], 'password' => Hash::make($input['password']),]);

        // $user->profile()->create([
        //     'phone'=>$input['phone'],
        // ]);


        // $team = Team::create([
        //     'owner_id' => $user->id,
        //     'name' => explode(' ', $user->name, 2)[0]."'s Team",
        // ]);
        // $team = Team::create([ 'owner_id' => $user->id, 'name' => explode(' ', $user->name, 2)[0]."'s Team", ]);

        // $team->profile()->create();

        // $user->attachTeam($team);

        // return $user;

    }

    /**
     * Create a personal team for the user. 
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    protected function createTeam(User $user)
    {
        $user->attachTeam($team = Team::create([
            'owner_id' => $user->id,
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            // 'personal_team' => true,
        ]));

        $team->profile()->create();
    }
}
