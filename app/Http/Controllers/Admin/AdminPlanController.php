<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StorePlanRequest;
use App\Http\Requests\UpdatePlanRequest;

class AdminPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('is super admin')) {
            return abort(401);
        }

        $plans = Plan::all();

        return view('admin.plans.index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('is super admin')) {
            return abort(401);
        }

        return view('admin.plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePlanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlanRequest $request)
    {
        if (! Gate::allows('is super admin')) {
            return abort(401);
        }

        // $plan->validate($request->all());
        if(Gate::denies('is-super-admin'))
        {
            abort(403, "You are not allowed to do this request.");
        }

        $plan = Plan::create([
            'name' => $request->name,
            //  Correction: abs(floor($data['price'] * 100))
            // 'price' => $request->price * 100,
            'price' => abs(floor($request->price * 100)),
            'duration' => $request->duration,
        ]);

        return redirect()->route('admin.plans.index')->with('success', "The plans <b>{$plan->name}</b> successfully created.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(Plan $plan)
    {
        if (! Gate::allows('is super admin')) {
            return abort(401);
        }

       return view('admin.plans.show', compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Plan $plan)
    {
        if (! Gate::allows('is super admin')) {
            return abort(401);
        }

        return view('admin.plans.show', compact('plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePlanRequest  $request
     * @param  \App\Models\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlanRequest $request, Plan $plan)
    {
        // $plan->validate($request->all());
        if(Gate::denies('is super admin'))
        {
            abort(403, "You are not allowed to do this request.");
        }

        $plan->update([
            'name' => $request->name,
            //  Correction: abs(floor($data['price'] * 100))
            // 'price' => $request->price * 100,
            'price' => abs(floor($request->price * 100)),
            'duration' => $request->duration,
        ]);

        return redirect()->route('admin.plans.index')->with('success', "The plans <b>{$plan->name}</b> successfully updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plan $plan)
    {
        if(Gate::denies('is super admin'))
        {
            abort(403, "You are not allowed to do this request.");
        }

        $plan->delete();

        return redirect()->route('admin.plans.index')->with('success', "The plan <b>{$plan->name}</b> successfully deleted.");
    }
}
