<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StorePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(401);
        }
        $permissions = Permission::all();

        return view('admin.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(401);
        }

        return view('admin.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePermissionRequest $request)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(401);
        }

        $permission = Permission::create($request->all());

        return redirect()->route('admin.permissions.index')->with('success', "The <b>$permission->name</b> successfully created.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(401);
        }

        return view('admin.permissions.show', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(401);
        }
        
        return view('admin.permissions.show', compact('permission'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePermissionRequest $request, Permission $permission)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(401);
        }

        $permission->update($request->all());

        return redirect()->route('admin.permissions.index')->with('success', "The <b>$permission->name</b> successfully updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(401);
        }

        $permission->delete();

        return redirect()->route('admin.permissions.index')->with('success', "The <b>$permission->name</b> succesfully deleted.");
    }

    /**
     * Mass remove the specified permission from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(401);
        }

        Permission::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
