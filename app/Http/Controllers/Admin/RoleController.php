<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(403);
        }
        $roles = Role::all();
        // $team = Team::findOrFail(auth()->user()->current_team_id);

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(403);
        }
        $permissions = Permission::get()->pluck('name','name');

        return view('admin.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(403);
        }

        $role = Role::create($request->except('permission'));
        $permissions = $request->input('permission') ? $request->input('permission') : [];
        $role->givePermissionTo($permissions);

        return redirect()->route('admin.roles.index')->with('success', "The role <b>$role->name</b> successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(403);
        }

        $role->load('permissions');
        $permissions = Permission::get()->pluck('name','name');

        return view('admin.roles.show', compact('role', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(403);
        }
        $permissions = Permission::get()->pluck('name','name');

        // dd($role, $permissions);
        return view('admin.roles.show', compact('role','permissions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(403);
        }

        $role->update($request->except('permissions'));
        $permissions = $request->input('permission') ? $request->input('permission') : [];
        $role->syncPermissions($permissions);
        // $team->updateRoleOfTeam($team,$request->role_id);

        return redirect()->route('admin.roles.index')->with('success', "The role <b>$role->name</b> successfully updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        if (! Gate::allows('is super admin'))
        {
            return abort(403);
        }

        $role->delete();

        return redirect()->route('admin.roles.index')->with('success', "The role <b>$role->name</b> successfully deleted!");
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        Role::whereIn('id', request('ids'))->delete();

        return response()->noContent();
    }
}
