<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(! Gate::allows('manage users'))
        {
            abort(401);
        }

        $users = User::all()->load('roles');
        

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(! Gate::allows('manage users'))
        {
            abort(401);
        }

        $roles = UserRole::get()->pluck('name', 'name');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        if(! Gate::allows('manage users'))
        {
            abort(401);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make(Str::random(8)), // TODO: Setup send email later on.. 
        ]);
        $user->profile()->create();

        $user->ownedTeams()->save($team = Team::forceCreate([
            'user_id' => $user->id,
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
        ]));

        $team->profile()->create();

        $user->assignRole($request->role);

        return redirect()->route('admin.users.index')->with('success', "User successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if(! Gate::allows('manage users'))
        {
            abort(401);
        }

        // $roles = UserRole::get()->pluck('name', 'name');
        $roles = $user->roles()->pluck('name', 'name');
        return view('admin.users.show', compact('user', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if(! Gate::allows('manage users'))
        {
            abort(401);
        }

        // $roles = $user->roles()->pluck('name', 'name');
        $roles = UserRole::get()->pluck('name', 'name');
        return view('admin.users.show', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        if(! Gate::allows('manage users'))
        {
            abort(401);
        }

        $user->update($request->all());
        $user->syncRoles($request->role);

        return redirect()->route('admin.users.index')->with('success', "User <b>$user->name</b> successfully updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if(! Gate::allows('manage users'))
        {
            abort(401);
        }

        $user->delete();

        return redirect()->route('admin.users.index')->with('success', "User <b>$user->name</b> successfully deleted.");
    }

}
