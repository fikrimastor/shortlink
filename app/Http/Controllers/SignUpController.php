<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\User;
use App\Models\Payment;
use Illuminate\Support\Str;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Support\Library\PaymentGateway\Billplz;
use App\Support\Library\PaymentGateway\SecurePay;

class SignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = Plan::all();

        return view('signup.index', compact('plans'));
    }

    /**
     * Show the form for subcriber choose payment gateway
     *
     * @return \Illuminate\Http\Response
     */
    public function review($id)
    {
        $plan = Plan::findOrFail($id);
        $user = User::find(Auth::user()->id)->load('profile');

        // dd();

        return view('signup.review', compact('plan','user'));
    }

    /**
     * To go payment process.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function go(Request $request, $payment_gateway)
    {
        $user = User::find(Auth::user()->id);
        $plan = Plan::find($request->plan_id);
        // dd($plan);

        switch($payment_gateway){
            case 'securepay':

                // Call payment gateway library
                $sp = new Securepay;

                // Prepare data before send to payment gateway
                $payment_data = [
                    'buyer_email' => $user->email,
                    'buyer_name' => $user->name,
                    'buyer_phone' => $user->profile->phone,
                    'callback_url' => config('app.url') . '/signup/callback/securepay',
                    'order_number' => Str::random(10),
                    'product_description' => 'Payment '.$plan->name.' for '.$user->name,
                    'redirect_url' => config('app.url') . '/signup/thankyou/securepay',
                    'transaction_amount' => number_format( ($plan->price / 100), 2, '.', '' ),
                    'duration' => $plan->duration,
                    'plan_id' => $plan->id

                ];

                // Create record in payment tables
                Payment::create([
                    'user_id' => $user->id,
                    'payment_gateway' => 'securepay',
                    'order_number' => $payment_data['order_number'],
                    'amount' => $payment_data['transaction_amount'],
                    'transaction_data' => json_encode($payment_data),
                    'status' => 'pending'
                ]);

                $sp->process($payment_data);


            break;
            case 'billplz':

                $billplz = new Billplz;
                $payment_data = [
                    'collection_id' => config('services.billplz.collection_id'),
                    'email' => $user->email,
                    'mobile' => $user->profile->phone,
                    'name' => $user->name,
                    'amount' => $plan->plan_price,
                    'order_number' => Str::random(10),
                    'description' => 'Payment '.$plan->name.' for '.$user->name,
                    'callback_url' => config('app.url') . '/signup/callback/billplz',
                    'redirect_url' => config('app.url') . '/signup/thankyou/billplz',
                    'duration' => $plan->duration,
                    'plan_id' => $plan->id

                ];

                Payment::create([
                    'user_id' => $user->id,
                    'payment_gateway' => 'billplz',
                    'order_number' => $payment_data['order_number'],
                    'amount' => $payment_data['amount'],
                    'transaction_data' => json_encode($payment_data),
                    'status' => 'pending'
                ]);

                // This billplz process will return payment link
                $payment_link = $billplz->process($payment_data);

                // redirect user to billplz payment link page
                return Redirect::away($payment_link);
            break;
            case 'bizappay':
                // Call bizappay client
                $bzp = new Bizappay;

                // Prepare data before send to payment gateway
                $payment_data = [
                    'buyer_email' => $user->email,
                    'buyer_name' => $user->name,
                    'buyer_phone' => $user->profile->phone,
                    'callback_url' => config('app.url') . '/signup/callback/bizappay',
                    'order_number' => Str::random(10),
                    'product_description' => 'Payment '.$plan->name.' for '.$user->name,
                    'redirect_url' => config('app.url') . '/signup/thankyou/bizappay',
                    'transaction_amount' => $plan->plan_price,
                    'duration' => $plan->duration,
                    'plan_id' => $plan->id

                ];

                // Create record in payment tables
                Payment::create([
                    'user_id' => $user->id,
                    'payment_gateway' => 'bizappay',
                    'order_number' => $payment_data['order_number'],
                    'amount' => $payment_data['transaction_amount'],
                    'transaction_data' => json_encode($payment_data),
                    'status' => 'pending'
                ]);

                $payment_link = $bzp->process($payment_data);

                // redirect user to billplz payment link page
                return Redirect::away($payment_link);

            break;
            case 'toyyibpay':
            break;
            case 'stripe':
            break;
        }
    }

    public function thankyou(Request $request, $payment_gateway)
    {
        switch($payment_gateway){
            case 'securepay':

                $verified = false;

                // Call payment gateway library
                $sp = new Securepay;
                if (isset($request->checksum)){
                    $verified = $sp->verify($request);
                }

                if($verified){
                    $payment = Payment::where('order_number', $request->order_number)->first();

                    $transaction_data = json_decode($payment->transaction_data);

                    if(($request->payment_status == 'true') && ($payment->status != 'paid')){
                        $payment->status = 'paid';
                        $payment->save();
                    }

                    /**
                     * First or create means
                     *
                     * 1) Search for existing subscription first, later will update the data into new
                     *
                     * 2) if no previous subscription we create new subscription.
                     */
                    $subscription = Subscription::firstOrCreate([
                        'user_id' => $payment->user_id,
                    ]);


                    /**
                     * - memberhsip dia memang dah expire
                     * --- expire_at = today + duration
                     *
                     * - membership dia belum expire
                     * --- expire_at = expire_at + duration
                     *
                     * - dia ahli baru
                     * --- expire_at = today + duration
                     */

                    $today = Carbon::now();

                    if(($subscription->expire_at == null) || ($subscription->expire_at->lessThan($today)))
                    {
                        $subscription->expire_at = $today->addDays($transaction_data->duration);
                        } else {
                        $subscription->expire_at = $today->addDays($transaction_data->duration);
                    }

                    $subscription->last_payment_id = $payment->id;

                    $subscription->save();

                }

            break;
            case 'billplz':
                echo 'success';
            break;
            case 'bizappay':
                $bzp = new Bizappay;

                $verified = $bzp->verify($request);

                if($verified){
                    $payment = Payment::where('bill_id', $request->billcode)->first();

                    // dd($payment);

                    $transaction_data = json_decode($payment->transaction_data);

                    if(($request->billstatus == '1') && ($payment->status != 'paid')){
                        $payment->status = 'paid';
                        $payment->save();
                    }

                    $subscription = Subscription::firstOrCreate([
                        'user_id' => $payment->user_id   
                    ]);

                    // dd($subscription);

                    $today = Carbon::now();

                    if(($subscription->expire_at == null) || ($subscription->expire_at->lessThan($today)))
                    {
                        $subscription->expire_at = $today->addDays($transaction_data->duration);
                    } else {
                        $subscription->expire_at = $subscription->expire_at->addDays($transaction_data->duration);
                    }

                    $subscription->last_payment_id = $payment->id;
                    // $subscription->update([
                    //     'last_payment_id' => $payment->id
                    // ]);

                    $subscription->save();

                    Log::info($request);
                }


            break;
            case 'toyyibpay':
            break;
            case 'stripe':
            break;

        }

        return view('signup.thankyou', compact('verified'));
    }

    public function callback(Request $request, $payment_gateway)
    {
        switch($payment_gateway){
            case 'securepay':

                // $response = $request->all([
                //     'billcode',
                //     'billamount',
                //     'billstatus',
                //     'billinvoice',
                //     'billtrans',
                //     'billstatusstring'
                // ]);

                    $payment = Payment::where('order_number', $request->order_number)->first();

                    $transaction_data = json_decode($payment->transaction_data);

                    if(($request->payment_status === 'true') && ($payment->status != 'paid')){
                        $payment->status = 'paid';
                        $payment->save();

                        /**
                         * First or create means
                         *
                         * 1) Search for existing subscription first, later will update the data into new
                         *
                         * 2) if no previous subscription we create new subscription.
                         */
                        $subscription = Subscription::firstOrCreate([
                            'user_id' => $payment->user_id,
                        ]);


                        /**
                         * - memberhsip dia memang dah expire
                         * --- expire_at = today + duration
                         *
                         * - membership dia belum expire
                         * --- expire_at = expire_at + duration
                         *
                         * - dia ahli baru
                         * --- expire_at = today + duration
                         */

                        $today = Carbon::now();

                        // Set the for format in carbon, to check if there is value or note in existing row in table subscription
                        $expire_at = new Carbon($subscription->expire_at);

                        if(($subscription->expire_at === null) || ($expire_at->lessThan($today)))
                        {
                            $subscription->expire_at = $today->addDays($transaction_data->duration);
                            } else {
                            $subscription->expire_at = $today->addDays($transaction_data->duration);
                        }

                        $subscription->user_id = $payment->user_id;
                        $subscription->last_payment_id = $payment->id;

                        $subscription->save();

                        Log::info($response);
                    } else {
                        Log::info($response);
                    }

            break;
            case 'billplz':
                echo 'success';
            break;
            case 'bizappay':
            break;

            $response = $request->all([
                    'billcode',
                    'billamount',
                    'billstatus',
                    'billinvoice',
                    'billtrans',
                    'billstatusstring'
                ]);

                $payment = Payment::where('bill_id', $request->bill_code)->first();

                $transaction_data = json_decode($payment->transaction_data);

                if(($request->billstatus === 1) && ($payment->status != 'paid')){
                    $payment->status = 'paid';
                    $payment->save();

                    $subscription = Subscription::firstOrCreate([
                        'user_id' => $payment->user_id   
                    ]);

                    $today = Carbon::now();

                    if(($subscription->expire_at === null) || ($expire_at->lessThan($today)))
                    {
                        $subscription->expire_at = $today->addDays($transaction_data->duration);
                    } else {
                        $subscription->expire_at = $subscription->expire_at->addDays($transaction_data->duration);
                    }

                    $subscription->last_payment_id = $payment->id;

                    $subscription->save();

                    Log::info($response);

                } else {
                    Log::info($response);
                }

            break;
            case 'toyyibpay':
            break;

        }

        Log::info($response);
    }


}
