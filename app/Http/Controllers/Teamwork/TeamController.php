<?php

namespace App\Http\Controllers\Teamwork;

use App\Models\Team;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Mpociot\Teamwork\TeamworkTeam;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use Mpociot\Teamwork\Exceptions\UserNotInTeamException;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = auth()->user()->teams->load('profile');
        // $profile = $team->profile;
        // return dd($teams);
        return view('teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('teams.create', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeamRequest $request)
    {
        $teamModel = config('teamwork.team_model');
        $team = $teamModel::create([
            'name' => $request->name,
            'owner_id' => $request->user()->getKey(),
        ]);
        $request->user()->attachTeam($team);

        $team->profile()->create([
            'phone' => $request->phone,
            'doe' => $request->doe,
            'address' => $request->address,
            'city' => $request->city,
            'description' => $request->description
        ]);

        return redirect(route('teams.index'));
    }

    /**
     * Switch to the given team.
     *
     * https://github.com/LaravelDaily/Laravel-Blog-Permissions-Course/blob/b186585bf3f3be9698fa3fd653ee44c432f3ff8e/app/Http/Controllers/JoinController.php
     * 
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function switchTeam($teamId)
    {
        // $teamModel = config('teamwork.team_model');
        $team = Team::whereUuid($teamId)->firstOrFail();
        $role = DB::table('team_user')
                ->where('team_id', $team->id)
                ->where('user_id', auth()->id())
                ->first();
        try {
            session([
                'team_id' => $team->id,
                'team_name' => $team->name,
                'team_role_id'=> $role->role_id
            ]);
            auth()->user()->switchTeam($team);
        } catch (UserNotInTeamException $e) {
            abort(403);
        }

        // return redirect(route('teams.index'));
        return back();
    }

    public function show(Request $request, $teamId)
    {
        $team = Team::whereUuid($teamId)->firstOrFail();
        $team->load('profile');
        return view('teams.show', compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($teamId)
    {    
        // $teamModel = config('teamwork.team_model');
        $team = Team::whereUuid($teamId)->load('profile');
        if (! auth()->user()->isOwnerOfTeam($team) || ! Gate::allows('teams manage')) {
            abort(403);
        }

        if (! auth()->user()->isOwnerOfTeam($team)) {
            abort(403);
        }

        // return dd($team);
        return view('teamwork.edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeamRequest $request, $teamId)
    {
        // $teamModel = config('teamwork.team_model');

        $team = Team::whereUuid($teamId)->firstOrFail();
        if (! auth()->user()->isOwnerOfTeam($team) 
        // || ! Gate::allows('teams manage')
        ) {
            abort(403);
        }
        
        $team->name = $request->name;
        // $team->description = $request->description;
        // $team->location = $request->location;
        $team->save();


        $team->profile()->update([
            'city' => $request->city,
            'description' => $request->description,
        ]);

        return redirect(route('teams.index'))->with('success', "The <b>$team->name</b> successfully updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($teamId)
    {
        $teamModel = config('teamwork.team_model');

        $teamA = Team::whereUuid($teamId)->firstOrFail();
        if (! auth()->user()->isOwnerOfTeam($teamA) || ! Gate::allows('teams manage')) {
            abort(403);
        }

        $teamA->delete();

        $userModel = config('teamwork.user_model');
        $userModel::where('current_team_id', $id)
                    ->update(['current_team_id' => null]);

        /**
         * Check if user have another team, if exist, switch to the first team found.
         *
         * This will do when the user delete the current team id.
         * 
         */
        if(auth()->user()->teams !== null)
        {
            $user = auth()->user();
            $team = $user->teams->first();
            $user->update(['current_team_id' => $team->id]);
        }

        return redirect(route('teams.index'))->with('success', "The role <b>$team->name</b> successfully deleted!");
    }
}