<?php

namespace App\Http\Controllers\Teamwork;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\PermissionRequest;
use Spatie\Permission\Models\Permission;

class TeamPermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('teams manage'))
        {
            return abort(401);
        }
        $permissions = Permission::all();

        return view('teamwork.permissions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gates::allows('teams manage'))
        {
            return abort(401);
        }

        return view('teamwork.permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        if (! Gate::allows('teams manage'))
        {
            return abort(401);
        }

        $permission = Permission::create($request->all());

        return redirect()->route('teams.permission.index')->with('success', "The <b>$permission->name</b>");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        if (! Gate::allows('teams manage'))
        {
            return abort(401);
        }

        return route('teams.permissions.show', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        if (! Gate::allows('teams manage'))
        {
            return abort(401);
        }
        
        return route('teams.permission.edit');
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, Permission $permission)
    {
        if (! Gate::allows('teams manage'))
        {
            return abort(401);
        }

        $permission->update($request->all());

        return redirect()->route('teams.permissions.index')->with('success', "Successfully updated");
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy(Permission $permission)
    // {
    //     if (! Gate::allows('teams manage'))
    //     {
    //         return abort(401);
    //     }

    //     $permission->delete();

    //     return redirect()->route('teams.permissions.index')->with('success', "Succesfully deleted.");
    // }

    // /**
    //  * Mass remove the specified permission from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function massDestroy(Request $request)
    // {
    //     if (! Gate::allows('teams manage'))
    //     {
    //         return abort(401);
    //     }

    //     Permission::whereIn('id', request('ids'))->delete();

    //     return response()->noContent();
    // }
}
