<?php

namespace App\Http\Controllers\Teamwork;

use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;

class TeamRoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Team $team)
    {
        if (! Gate::allows('teams manage'))
        {
            return abort(403);
        }
        $roles = Role::all();
        // $team = Team::findOrFail(auth()->user()->current_team_id);

        return view('teamwork.roles.index', compact('roles', 'team'));
    }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    //     if (! Gates::allows('teams manage'))
    //     {
    //         return abort(403);
    //     }
    //     $permissions = Permission::get()->pluck('name','name');

    //     return view('teamwork.roles.create', compact('permissions'));
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(StoreRoleRequest $request)
    // {
    //     if (! Gate::allows('teams manage'))
    //     {
    //         return abort(403);
    //     }

    //     $role = Role::create($request->accept('permission'));
    //     $permissions = $request->input('permission') ? $request->input('permission') : [];
    //     $role->givePermissionTo($permissions);

    //     return redirect()->route('teams.roles.index')->with('success', "The role <b>$role->name</b> successfully created!");
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        if (! Gate::allows('teams_manage'))
        {
            return abort(403);
        }

        $role->load('permissions');

        return view('teamwork.roles.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        if (! Gate::allows('teams_manage'))
        {
            return abort(403);
        }
        $permissions = Permission::get()->pluck('name','name');

        return view('teamwork.roles.edit', compact('role','permissions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Team $team)
    {
        if (! Gate::allows('teams_manage'))
        {
            return abort(403);
        }

        // $role->update($request->except('permissions'));
        // $permissions = $request->input('permission') ? $request->input('permission') : [];
        // $role->syncPermissions($permissions);
        $team->updateRoleOfTeam($team,$request->role_id);

        return redirect()->route('teams.roles.index')->with('success', "The role <b>$role->name</b> successfully updated!");
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy(Role $role)
    // {
    //     if (! Gate::allows('is super admin'))
    //     {
    //         return abort(403);
    //     }

    //     $role->delete();

    //     return redirect()->route('teams.roles.index')->with('success', "The role <b>$role->name</b> successfully deleted!");
    // }

    // /**
    //  * Delete all selected Role at once.
    //  *
    //  * @param Request $request
    //  */
    // public function massDestroy(Request $request)
    // {
    //     Role::whereIn('id', request('ids'))->delete();

    //     return response()->noContent();
    // }
}
