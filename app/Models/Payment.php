<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable =[
        'user_id',
        'payment_gateway',
        'order_number',
        'amount',
        'transaction_data',
        'status',
        'bill_id',
        'payment_link',
    ];
}
