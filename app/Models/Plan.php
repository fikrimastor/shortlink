<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'price',
        'duration',
    ];

    public function getMoneyPriceAttribute()
    {
        return 'RM ' . number_format( ($this->price / 100), 2, '.', '' );
    }

    public function getPlanPriceAttribute()
    {
        return number_format( ($this->price / 100), 2, '.', '' );
    }
}
