<?php

namespace App\Models;

use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Mpociot\Teamwork\Traits\UsedByTeams;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Profile extends Model
{
    use HasFactory;

    protected $fillable =[
        // 'profileable_id',
        // 'profileable_type',
        'firstname',
        'lastname',
        'phone',
        'gender',
        'dob',
        'doe',
        'address',
        'city',
        'description',
    ];

    public function profileable()
    {
        return $this->morphTo(__FUNCTION__, 'profileable_type', 'profileable_id');
    }
}
