<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    protected $fillable =[
        'user_id',
        'expire_at',
        'last_payment_id',
    ];

    protected $casts = [
        'expire_at' => 'datetime'
    ];

    public function getActiveAttribute()
    {
        return $this->expire_at->greaterThan(Carbon::now());
    }

    public function getDaysLeftAttribute()
    {
        if ($this->active)
        {
            return $this->expire_at->diffInDays();
        }

        return 0;
    }
}
