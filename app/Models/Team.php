<?php

namespace App\Models;

use App\Models\Team;
use App\Models\Profile;
use Illuminate\Support\Str;
use Mpociot\Teamwork\TeamworkTeam;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Team extends TeamworkTeam
{
    use HasFactory, HasRoles;

    protected $fillable= [
        'owner_id',
        'name',
        'description',
        'location',
    ];

    public static function booted()
    {
        static::creating(function(Team $team) {
            $team->uuid = Str::uuid();
            // $team->profile()->create(['profileable_id'=> $team->id]);
        });
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'team_id')->withTimestamps()->orderBy('team_id');
    }

    // /**
    //  * Get the profile associated with the Team
    //  *
    //  * @return \Illuminate\Database\Eloquent\Relations\HasOne
    //  */
    public function profile()
    {
        // return $this->hasOne(Profile::class, 'team_id');
        return $this->morphOne(Profile::class, 'profileable');
    }

    // Find user with role name in teams.
    public function usersWithRole($role) {
        return $this->users()->whereHas('roles', function($query) use($role) {
            $query->where('name', $role);
        })->with('roles')->get();
    }

    // public function getRoleIdAttribute()
    // {
    //     if(session('team_role_id')){
    //         return session('team_role_id');
    //     }

    //     return $this->attribute['role_id'];
    // }

    // public function getTeamIdAttribute()
    // {
    //     $team = $this->teams()->first();
    //     if($team){
    //         session([
    //             'team_id' => $team->id,
    //             'team_uuid' => $team->uuid,
    //             'team_name' => $team->name,
    //             'team_city' => $team->profile->city,
    //             'team_description' => $team->profile->description,
    //             'team_role_id' => $team->pivot->role_id,
    //         ]);
    //     }

    //     return $team->id;
    // }
}
