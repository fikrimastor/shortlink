<?php

namespace App\Models;

use App\Models\Team;
use App\Models\Profile;
use App\Traits\UserHasTeams;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, UserHasTeams, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'current_team_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getTeamRolesAttribute()
    {
        return $this->with('teams.roles')->teams->pluck('roles.name')->flatten()->unique()->toArray();
    }

    public function teamRoles()
    {
        return $this->belongsToMany(Team::class, 'team_user', 'user_id', 'team_id')->withPivot('role_id')->withTimestamps()->orderBy('role_id');
    }


    // /**
    //  * Get the profile associated with the User
    //  *
    //  * @return \Illuminate\Database\Eloquent\Relations\HasOne
    //  */
    public function profile()
    {
        // return $this->hasOne(Profile::class);
         return $this->morphOne(Profile::class, 'profileable');
    }
    // public function profile()
    // {
    //     return $this->morphOne(Profile::class, 'profileable');
    // }

    public function withTeam()
    {
        return $this->belongsToMany(Team::class)->withPivot('role_id')->withTimestamps();
    }

    public function teamWithRole($team)
    {
        $role_id = $this->withTeam()->where('team_id', $team)->first()->pivot->role_id;

        $role = Role::find($role_id);
        return $role->name;
    }

    // Assign team and role to user
    public function assignTeamAndRole($team,$role)
    {
        return $this->attachTeam($team, $pivot=['role_id'=>$role]);
    }

    // Remove team and role from user
    public function detachTeamAndRole($team,$role)
    {
        return $this->detachTeam($team, ['role_id'=>$role]);
    }

    public function updateRoleOfTeam($team,$role)
    {
        return $this->teams()->where('team_id', $team)->update(['role_id'=>$role]);
    }
}
