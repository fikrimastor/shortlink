<?php

namespace App\Support\Library\PaymentGateway;

use Duit\MYR;
use Billplz\Client;
use Billplz\Signature;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class Billplz
{
    private $collection_id;
    private $api_key;
    private $x_signature_key;
    private $url = '';

    function __construct()
    {
        $this->url = 'https://www.' . (config('services.billplz.env') == 'sandbox' ? 'billplz-sandbox' : 'billplz') . '.com/api/v3/bills';
        $this->api_key = config('services.billplz.api_key');
        $this->x_signature_key = config('services.billplz.x_signature_key');
        $this->collection_id = config('services.billplz.collection_id');


    }

    public function process($payment_data)
    {
        if (! $this->x_signature_key)
            {
                $billplz = Client::make($this->api_key);
            } else {
                $billplz = Client::make($this->api_key, $this->x_signature_key);
            }

            if (config('services.billplz.env') == 'sandbox') {
                $billplz->useSandbox();
                $billplz->useVersion('v3');
            } else {
                 $billplz->useVersion('v3');
            }

            $response = $billplz->bill()->create(
                $this->collection_id,
               $payment_data['email'],
                $payment_data['mobile'],
                $payment_data['name'],
                MYR::given($plan->price),
                $payment_data['callback_url'],
                $payment_data['description'],
                [
                    'redirect_url' => $payment_data['redirect_url']
                ]
            );
             if ($response->getStatusCode() == 200)
            {
                $final = $response->toArray();
            } else {
                abort(403);
            }

            Payment::where('order_number', $payment_data['order_number'])->update([
                'bill_id' => $final['id'],
                'payment_link' =>$final['url'],
            ]);


        $payment_link = $final['url'];

        // return Redirect::to($final['url']);
        return $payment_link;
        // return Redirect::action('SignUpController@pay', compact('payment_link'));

        // Testing purpose
        // $response = $bill->create('inbmmepb','api@billplz.com',null,'Michael API V3',\Duit\MYR::given(200),'http://example.com/webook/','Maecenas eu placerat ante.',['redirect_url' => 'http://example.com/redirect/']);

    }

    public function verify(Request $request)
    {

        $data = $request->all();
        ksort($data);

        $x_signature = $data['x_signature'];
        unset($data['x_signature']);

        $string = Signature::create($data);

        $sign = Signature::verify($data, $this->x_signature);

        return ($sign == $x_signature);

        // dd($data, $string, $checksum, $sign);
    }
}
