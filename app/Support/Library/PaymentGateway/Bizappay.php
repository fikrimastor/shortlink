<?php

namespace App\Support\Library\PaymentGateway;

use Billplz\Signature;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;


class Bizappay
{
    private $category_id;
    private $api_key;
    private $url = '';
    private $token_url = '';

    function __construct()
    {
        $this->bill_info = 'https://' . (config('services.bizappay.env') == 'sandbox' ? 'stg.' : '') . 'bizappay.my/api/v3/bill/info';
        $this->token_url = 'https://' . (config('services.bizappay.env') == 'sandbox' ? 'stg.' : '') . 'bizappay.my/api/v3/token';
        $this->url = 'https://' . (config('services.bizappay.env') == 'sandbox' ? 'stg.' : '') . 'bizappay.my/api/v3/bill/create';
        $this->api_key = config('services.bizappay.api_key');
        $this->category_id = config('services.bizappay.category_id');
                
    }

    public function process($payload)
    {
        $post_data = [
            'apiKey' => $this->api_key,
            'category' => $this->category_id,
            'name' => $payload['product_description'],
            'amount' => $payload['transaction_amount'],
            'payer_name' => $payload['buyer_name'],
            'payer_email' => $payload['buyer_email'],
            'payer_phone' => $payload['buyer_phone'],
            'webreturn_url' => $payload['redirect_url'],
            'callback_url' => $payload['callback_url'],
            'ext_reference' => $payload['order_number']
        ];

        // dd($post_data);
        $generate_token = Http::asForm()->post($this->token_url, [
            // 'apiKey' => $this->api_key
            'apiKey' => $this->api_key
        ]);
        
        // echo $generate_token;
        // dd($generate_token);

        $response = Http::withHeaders([
            'Authentication' => $generate_token['token']
        ])->asForm()->post($this->url, $post_data);

        // dd($response);

        Payment::where('order_number', $payload['order_number'])->update([
            'bill_id' => $response['billCode'],
            'payment_link' =>$response['url'],
        ]);

        // dd($response['url']);
        return $response['url'];

    }

    public function verify(Request $request)
    {

        $data = $request->all();
        // ksort($data);
        // dd($data);

        // $x_signature = $data['x_signature'];
        // unset($data['x_signature']);

        // $string = Signature::create($data);

        // $sign = Signature::verify($data, $this->x_signature);
        $payment = Payment::where('order_number', $data['billcode'])->first();

        // Collect data from request
        $post_data = [
            'apiKey' => $this->api_key,
            'search_str' => $request->billcode,
            'payment_status' => 1
        ];

        // Send post form request 
        $generate_token = Http::asForm()->post($this->token_url, [
            // 'apiKey' => $this->api_key
            'apiKey' => $this->api_key
        ]);

        // Send request for bill status
        $response = Http::withHeaders([
            'Authentication' => $generate_token['token']
        ])->asForm()->post($this->bill_info, $post_data)->json(['bill']);

        // dd(json_decode($response->getBody()));
        // echo $response->toArray();

        // dd($response['payments'][0]['status']);
        

        // $final = json_decode($response->getBody());

        // $final->

        // Set parameters to 
        $sign = $request->billstatus;
        
        $x_sign = $response['payments'][0]['status'];

        return ($sign == $x_sign);

        // dd($data, $string, $checksum, $sign);
    }
}
