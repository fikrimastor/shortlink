<?php

namespace Database\Seeders;

use App\Models\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Plan
        Plan::create(['name' => 'Free', 'price' => 0, 'duration' => 365]);
        Plan::create(['name' => 'Monthly', 'price' => 5000, 'duration' => 30]);
        Plan::create(['name' => 'Yearly', 'price' => 50000, 'duration' => 365]);
    }
}
