<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Admin users
        $user = \App\Models\User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
        ]);
        
        $team = Team::create([
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            'owner_id' => $user->id,
        ]);
        
        $role = Role::where('name', 'super-admin')->first();
        $user->syncRoles($role);
        $user->attachTeam(1, ['role_id' => $role->id]);
        $user->profile()->create();
        // $user->assignRole(['role_id' => $role->id, $user->current_team_id]);
        $team->profile()->create();

        // Create Manager User
        $user = \App\Models\User::factory()->create([
            'name' => 'Example Moderator User',
            'email' => 'moderator@example.com',
            'password' => Hash::make('password'),
        ]);
        
        $team = Team::create([
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            'owner_id' => $user->id,
        ]);
        
        $role = Role::where('name', 'manager')->first();
        $user->syncRoles($role);
        $user->attachTeam(1, ['role_id' => $role->id]);
        $user->profile()->create();
        // $user->assignRole(['role_id' => $role->id, 'team_id' => $user->current_team_id]);
        $team->profile()->create();

        // Create member user
        $user = \App\Models\User::factory()->create([
            'name' => 'Example User',
            'email' => 'user@example.com',
            'password' => Hash::make('password'),
        ]);
        
        $team = Team::create([
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            'owner_id' => $user->id,
        ]);
        
        $role = Role::where('name', 'member')->first();
        $user->syncRoles($role);
        $user->attachTeam(1, ['role_id' => $role->id]);
        $user->profile()->create();
        // $user->assignRole(['role_id' => $role->id, 'team_id' => $user->current_team_id]);
        $team->profile()->create();

    }
}
