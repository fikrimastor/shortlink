const a = document.getElementById("url");
const b = document.getElementById("action");
// const c = document.getElementById("value");
const inputs = document.querySelectorAll('.radiogroup');

// A simple function to handle the click event for each input.
const clickHandler = i => {
  a.href = i.getAttribute("data-url");
  b.action = i.getAttribute("data-url");
  // c.value = i.getAttribute("value");
  a.textContent = i.getAttribute("value");
};

// Possibly even less code again.
inputs.forEach(i => i.onclick = () => clickHandler(i));
