@extends('layouts.private',[
'activePage' => 'planIndex',
'parentPage' => 'Plan',
'menuParent' => '',
'title' => __('Plans')
])
@section('content')
<div class="container-fluid py-4">
    <section class="py-3">
        @if( session('success') )
        @include('layouts.alerts.feedback')
        @endif
        <div class="row mb-4 mb-md-0">
            <div class="col-md-8 me-auto my-auto text-left">
                <h5>List of All Your Plans</h5>
                <p>You can see all your plans here. <br> You may edit, view or switch plan from this page.</p>

            </div>
            <div class="col-lg-4 col-md-12 my-auto text-end">
                {{-- <button type="button" class="btn bg-gradient-primary mb-0 mt-0 mt-md-n9 mt-lg-0"
                    href="{{route('admin.plans.create')}}">

                </button> --}}
                <a href="{{route('admin.plans.create')}}" class="btn bg-gradient-warning mb-0 mt-0 mt-md-n9 mt-lg-0">
                    <i class="material-icons text-white position-relative text-md pe-2">add</i>{{__('Create Plan')}}
                </a>
            </div>
        </div>

        <div class="row mt-lg-4 mt-2">
            @foreach ($plans as $plan)
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card">
                    <div class="card-body p-3">
                        <div class="d-flex mt-n2">
                            {{-- <div class="avatar avatar-xl bg-gradient-dark border-radius-xl p-2 mt-n4">
                                <img src="../../../assets/img/small-logos/logo-slack.svg" alt="slack_logo">
                            </div> --}}
                            <h6 class="mb-0">{{$plan->name}}</h6>
                            {{-- <div class="ms-3 my-auto">
                                <div class="avatar-group">
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Jessica Rowland">
                                        <img alt="Image placeholder" src="../../../assets/img/plan-3.jpg" class="">
                                    </a>
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Audrey Love">
                                        <img alt="Image placeholder" src="../../../assets/img/plan-4.jpg"
                                            class="rounded-circle">
                                    </a>
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Michael Lewis">
                                        <img alt="Image placeholder" src="../../../assets/img/plan-2.jpg"
                                            class="rounded-circle">
                                    </a>
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Jessica Rowland">
                                        <img alt="Image placeholder" src="../../../assets/img/plan-3.jpg" class="">
                                    </a>
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Audrey Love">
                                        <img alt="Image placeholder" src="../../../assets/img/plan-4.jpg"
                                            class="rounded-circle">
                                    </a>
                                </div>
                            </div> --}}
                            <div class="ms-auto">
                                <div class="dropdown">
                                    <button class="btn btn-link text-secondary ps-0 pe-2" id="navbarDropdownMenuLink"
                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v text-lg"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-end me-sm-n4 me-n3"
                                        aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="{{route('admin.plans.show', $plan->id)}}"><i
                                                class="fa fa-pencil"></i> Edit</a>
                                        {{-- <a class="dropdown-item" href="{{route('plans.destroy', $plan)}}">
                                            <i class="fa fa-trash-o"></i>{{__('Delete')}}
                                            <form style="display: inline-block;"
                                                action="{{route('plans.destroy', $plan)}}" method="post"
                                                class="btn btn-sm btn-default">
                                                @csrf
                                                <input type="hidden" name="_method" value="DELETE" />
                                            </form> --}}
                                            {{-- TODO:Make a modal --}}
                                            {{-- <a class="dropdown-item" href="{{route('plans.edit', $plan)}}"><i
                                                    class="fa fa-pencil"></i> Edit</a> --}}
                                            <a class="dropdown-item"
                                                href="{{ route('admin.plans.destroy', $plan->id) }}" onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();"><i
                                                    class="fa fa-trash-o"></i>
                                                {{__('Delete
                                                Plans')}}</a>
                                            <form id="delete-form"
                                                action="{{ route('admin.plans.destroy', $plan->id) }}" method="POST"
                                                class="d-none">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-sm mt-3"> Price:
                            {{$plan->money_price}}
                        </p>
                        <p class="text-sm mt-3"> Duration:
                            {{$plan->duration}} Days
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100 ">
                    <div class="card-body d-flex flex-column justify-content-center text-center">
                        <a href="{{route('admin.plans.create')}}" class="mb-1 mt-1 mt-md-n9 mt-lg-0 text-center">
                            <i class="material-icons text-secondary">group_add</i>
                            <h5 class="text-secondary">
                                {{__('Create Plan')}}
                            </h5>
                        </a>
                        <br />
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-lg-4 mt-2">

        </div>
    </section>
    @include('layouts.footers.auth')
</div>
@endsection
