@extends('layouts.private',[
'activePage' => 'planEdit',
'parentPage' => 'Plan',
'menuParent' => 'admin.plans.index',
'title' => __('Plans')
])

@section('content')
<div class="container-fluid py-4">
    <div class="row mt-4">
        <div class="col-lg-9 col-12 mx-auto position-relative">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-success shadow text-center border-radius-xl mt-n4 me-3 float-start">
                        <i class="material-icons opacity-10">groups</i>
                    </div>
                    <h6 class="mb-0">{{__('Edit Plan')}} {{$plan->name}}</h6>
                </div>
                <div class="card-body pt-2">
                    <form class="form-horizontal" method="post" action="
                    {{-- {{route('admin.plans.update', $plan)}} --}}
                    ">
                        @method('PUT')
                        @csrf
                        <div class="form-group @error('name') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="planName">{{__('Plan Name')}}</label>
                                <input type="text" class="form-control" id="planName" name="name"
                                    value="{{ old('name', $plan->name) }}">
                                @error('name')
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mt-4 @error('price') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="planPrice">{{__('Plan price')}}</label>
                                <input type="number" class="form-control" id="planPrice" name="price"
                                    value="{{ old('price', $plan->money_price) }}">
                                @error('price')
                                <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mt-4 @error('duration') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="planDuration">{{__('Plan Duration')}}</label>
                                <input type="number" class="form-control" id="planDuration" name="duration"
                                    value="{{ old('duration', $plan->duration) }}">
                                @error('duration')
                                <span class="help-block">
                                    <strong>{{ $errors->first('duration') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="d-flex justify-content-end mt-4">
                            <a href="{{route('admin.plans.index', $plan)}}"
                                class="btn btn-light m-0">{{__('Cancel')}}</a>
                            <button type="Submit" name="button" class="btn bg-gradient-success m-0 ms-2"><i
                                    class="fa fa-btn fa-save"></i>{{__('SAve')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('layouts.footers.auth') --}}
</div>
@endsection
