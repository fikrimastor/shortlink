@extends('layouts.private',[
'activePage' => 'roleCreate',
'parentPage' => 'Role',
'menuParent' => 'admin.roles.index',
'title' => __('Create Roles')
])

@section('content')
<div class="container-fluid py-4">
    <div class="row mt-4">
        <div class="col-lg-9 col-12 mx-auto position-relative">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-success shadow text-center border-radius-xl mt-n4 me-3 float-start">
                        <i class="material-icons opacity-10">groups</i>
                    </div>
                    <h6 class="mb-0">{{__('Create Role')}}</h6>
                </div>
                <div class="card-body pt-2">
                    <form class="form-horizontal" method="post" action="{{route('admin.roles.store')}}">
                        @csrf
                        <div class="form-group @error('name') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="roleName">{{__('Role Name')}}</label>
                                <input type="text" class="form-control" id="roleName" name="name"
                                    value="{{ old('name') }}">
                                @error('name')
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="d-flex justify-content-end mt-4">
                            {{-- <button type="button" name="button" class="btn btn-light m-0" href=""></button> --}}
                            <a href="{{route('admin.roles.index')}}" class="btn btn-light m-0">{{__('Cancel')}}</a>
                            <button type="Submit" name="button" class="btn bg-gradient-success m-0 ms-2">{{__('Create
                                Role')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('layouts.footers.auth') --}}
</div>
@endsection
