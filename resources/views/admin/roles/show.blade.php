@extends('layouts.private',[
'activePage' => 'roleEdit',
'parentPage' => 'Role',
'menuParent' => 'admin.roles.index',
'title' => __('Roles')
])

@section('content')
<div class="container-fluid py-4">
    <div class="row mt-4">
        <div class="col-lg-9 col-12 mx-auto position-relative">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-success shadow text-center border-radius-xl mt-n4 me-3 float-start">
                        <i class="material-icons opacity-10">groups</i>
                    </div>
                    <h6 class="mb-0">{{__('Edit Roles')}} {{$role->name}}</h6>
                </div>
                <div class="card-body pt-2">
                    <form class="form-horizontal" method="post" action="
                    {{route('admin.roles.update', $role)}}
                    ">
                        @method('PUT')
                        @csrf
                        <div class="form-group @error('name') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="roleName">{{__('Roles Name')}}</label>
                                <br />
                                <input type="text" class="form-control" id="roleName" name="name"
                                    placeholder="{{ old('name', $role->name) }}" value="{{ old('name', $role->name) }}">
                                @error('name')
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('permission') ? 'has-error' : '' }}">
                            {{-- <label for="permission">{{__('Roles Name')}}*
                                <span class="btn btn-info btn-xs select-all">{{__('Select All')}}</span>
                                <span class="btn btn-info btn-xs deselect-all">{{__('Deselect All')}}</span></label>
                            <select name="permission[]" id="permission" class="form-control select2" multiple="multiple"
                                required>
                                @foreach($permissions as $id => $permissions)
                                <option value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) &&
                                    $role->permissions()->pluck('name', 'id')->contains($id)) ? 'selected' : '' }}>{{
                                    $permissions }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('permission'))
                            <em class="invalid-feedback">
                                {{ $errors->first('permission') }}
                            </em>
                            @endif --}}
                            <div class="custom-control custom-checkbox small mt-4">
                                <h6>{{__('Permissions')}}</h6>
                                @foreach($permissions as $id => $permissions)
                                <input @if( in_array($id, old('permissions', [])) || isset($role) &&
                                    $role->permissions()->pluck('name', 'id')->contains($id))
                                checked
                                @endif
                                type="checkbox" class="custom-control-input"
                                name="permission[]" value="{{$permissions}}" id="permission_{{$permissions}}">
                                <label class="custom-control-label"
                                    for="permission_{{$permissions}}">{{$permissions}}</label><br />
                                @endforeach
                            </div>
                        </div>

                        <div class="d-flex justify-content-end mt-4">
                            <a href="{{route('admin.roles.index', $role)}}"
                                class="btn btn-light m-0">{{__('Cancel')}}</a>
                            <button type="Submit" name="button" class="btn bg-gradient-success m-0 ms-2"><i
                                    class="fa fa-btn fa-save"></i> {{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('layouts.footers.auth') --}}
</div>
@endsection
