@extends('layouts.private',[
'activePage' => 'userIndex',
'parentPage' => 'User',
'menuParent' => '',
'title' => __('Users')
])
@section('content')
<div class="container-fluid py-4">
    <section class="py-3">
        @if( session('success') )
        @include('layouts.alerts.feedback')
        @endif
        <div class="row mb-4 mb-md-0">
            <div class="col-md-8 me-auto my-auto text-left">
                <h5>List of All Your Users</h5>
                <p>You can see all your users here. <br> You may edit, view user from this page.</p>

            </div>
            <div class="col-lg-4 col-md-12 my-auto text-end">
                {{-- <button type="button" class="btn bg-gradient-primary mb-0 mt-0 mt-md-n9 mt-lg-0"
                    href="{{route('admin.users.create')}}">

                </button> --}}
                <a href="{{route('admin.users.create')}}" class="btn bg-gradient-warning mb-0 mt-0 mt-md-n9 mt-lg-0">
                    <i class="material-icons text-white position-relative text-md pe-2">add</i>{{__('Create
                    User')}}
                </a>
            </div>
        </div>
        <div class="card shadow mt-lg-4 mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('List Of Users')}}</h6>
            </div>
            <div class="card-body">
                <table class="table table-stripe">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">NAMA</th>
                            <th class="text-center">ROLE</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td class="text-center">{{$user->id}}</td>
                            <td class="text-center">{{$user->name}}</td>
                            <td class="text-center">@foreach ($user->roles()->pluck('name') as $role)
                                <span class="badge badge-info">{{ $role }}</span><br />
                                @endforeach
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.users.edit', $user->id) }}"
                                    class="btn btn-sm btn-secondary">EDIT</a>

                                <form method="post" class="d-inline"
                                    action="{{ route('admin.users.destroy', $user->id) }}"
                                    onsubmit="return confirm('Are you sure you want to delete {{$user->name}} ({{$user->id}})?');">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </section>
    @include('layouts.footers.auth')
    <div class="modal fade" id="modal-delete" tabindex="-1" user="dialog" aria-labelledby="modal-delete"
        aria-hidden="true">
        <div class="modal-dialog modal-danger modal-dialog-centered modal-" user="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title font-weight-normal" id="modal-title-delete">{{__('Delete Users')}}
                    </h6>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="py-3 text-center">
                        <i class="material-icons h1 text-secondary">
                            notifications_active
                        </i>
                        <h5 class="text-gradient text-danger mt-4">Are you sure you want to delete this
                            {{$user->name}}
                            record?</h5>
                        <p>If you delete this, it will be gone forever.
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link text-secondary "
                        data-bs-dismiss="modal">{{__('Cancel')}}</button>
                    <form action="{{route('admin.users.destroy', $user)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="_method" value="DELETE" />
                        <button type="submit" class="btn btn-success ml-auto">{{__('Yes,
                            Delete It')}}</button>
                        {{-- <button class="btn btn-danger btn-sm">
                            Delete</button> --}}
                        {{-- <a class="dropdown-item" href="javascript:;"
                            onclick="material.showSwal('warning-message-and-confirmation')"><i
                                class="fa fa-trash-o"></i>
                            {{__('Delete')}} --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
