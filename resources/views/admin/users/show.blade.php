@extends('layouts.private',[
'activePage' => 'userEdit',
'parentPage' => 'User',
'menuParent' => 'admin.users.index',
'title' => __('Users')
])

@section('content')
<div class="container-fluid py-4">
    <div class="row mt-4">
        <div class="col-lg-9 col-12 mx-auto position-relative">
            <div class="card">
                <div class="card-header p-3 pt-2">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-success shadow text-center border-radius-xl mt-n4 me-3 float-start">
                        <i class="material-icons opacity-10">groups</i>
                    </div>
                    <h6 class="mb-0">{{__('Edit Users')}} {{$user->name}}</h6>
                </div>
                <div class="card-body pt-2">
                    <form class="form-horizontal" method="post" action="{{route('admin.users.update', $user)}}">
                        @method('PUT')
                        @csrf
                        <div class="form-group @error('name') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="userName">{{__('Users Name')}}</label>
                                <br />
                                <input type="text" class="form-control" id="userName" name="name"
                                    placeholder="{{ old('name', $user->name) }}" value="{{ old('name', $user->name) }}">
                                @error('name')
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mt-4 @error('email') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="userEmail">{{__('Users Email')}}</label>
                                <br />
                                <input type="email" class="form-control" id="userEmail" name="email"
                                    placeholder="{{ old('email', $user->email) }}" value="{{ old('email', $user->email) }}">
                                @error('email')
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mt-4 @error('role') has-error text-danger @enderror">
                            <div class="custom-control custom-checkbox small">
                                <label for="userRole">{{__('User Roles:')}}</label><br/>
                                @foreach($roles as $id => $roles)
                                <input id="UserRole" @if (in_array($id, old('roles', []))  || isset($user) &&
                                    $user->roles()->pluck('name', 'id')->contains($id))
                                    checked
                                @endif
                                type="checkbox" class="custom-control-input"
                                name="role[]" value="{{$id}}" id="role_{{$roles}}">
                                <label class="custom-control-label"
                                    for="role_{{$roles}}">{{$roles}}</label><br/>
                                @endforeach
                            </div>
                        </div>

                        <div class="d-flex justify-content-end mt-4">
                            <a href="{{route('admin.users.index', $user)}}"
                                class="btn btn-light m-0">{{__('Cancel')}}</a>
                            <button type="Submit" name="button" class="btn bg-gradient-success m-0 ms-2"><i
                                    class="fa fa-btn fa-save"></i>{{__(' Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('layouts.footers.auth') --}}
</div>
@endsection
