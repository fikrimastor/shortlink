@extends('layouts.public')
@section('content')

<div class="page-header align-items-start min-vh-100"
    style="background-image: url('/material/assets/img/chicken-login.jpg');">
    <span class="mask bg-gradient-dark opacity-6"></span>
    <div class="container my-auto">
        <div class="row">
            <div class="col-lg-4 col-md-8 mx-auto">
                <div class="card z-index-0">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-light shadow-success border-radius-lg py-3 pe-1 text-center py-4">
                            <img src="/logo-veegro.png" class="navbar-brand-img h-10 w-50" alt="main_logo">
                            <h4 class="font-weight-bolder text-dark mt-1">Reset Password</h4>
                            <p class="mb-1 text-sm text-dark">Enter your email to reset your password</p>
                        </div>
                    </div>
                    <div class="card-body">
                        <form role="form" class="text-start" method="POST" action="{{ route('password.email') }}">
                            {{-- <input type="hidden" name="token" value="{{ session('status') }}"> --}}
                            @csrf
                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        placeholder="{{ __('Email') }}" type="email" name="email"
                                        value="{{ old('email') }}" required autofocus>
                                </div>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn bg-gradient-success w-100 mt-3 mb-0">{{ __('Email Password Reset Link') }}</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer text-center pt-0 px-lg-2 px-1">
                        <p class="mb-4 text-sm mx-auto">
                            Don't have an account?
                            <a href="{{route('register')}}" class="text-success text-gradient font-weight-bold">{{ __('Sign
                                up') }}</a>
                        </p>
                        <p class="mb-4 text-sm mx-auto">
                            Back to 
                            <a href="{{route('login')}}" class="text-success text-gradient font-weight-bold">{{ __('Sign
                                in') }}</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
