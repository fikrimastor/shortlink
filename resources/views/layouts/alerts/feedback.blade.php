<div class="alert alert-success alert-dismissible fade show" role="alert">
    <span class="alert-icon text-white"><i class="ni ni-like-2"></i></span>
    <span class="alert-text text-white"><strong>Success!</strong> {!! session('success') !!}</span>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
