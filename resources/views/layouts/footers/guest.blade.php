<footer class="footer {{ $activePage == 'home' ? 'position-absolute' : ''}} bottom-2 py-0 w-100">
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto text-center mt-1">
                <p class="mb-0 {{ $activePage == 'home' ? 'text-white' : 'text-dark'}}">
                    Copyright © 2017-<script>
                        document.write(new Date().getFullYear())
                    </script> Made with <i class="fa fa-heart" aria-hidden="true"></i> by
                    <a href="https://www.serotech.com.my"
                        class="font-weight-bold {{ $activePage == 'home' ? 'text-white' : 'text-dark'}}"
                        target="_blank">Serotech
                        Sdn. Bhd.</a> for a better farm.
                </p>
            </div>
        </div>
    </div>
</footer>
