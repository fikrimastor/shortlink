<aside
    class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 ps ps--active-y bg-white"
    id="sidenav-main" data-color="success">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer opacity-5 position-absolute end-0 top-0 d-xl-none text-dark"
            aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="{{ route('dashboard') }}">
            <img src="/logo-veegro.png" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold text-dark"></span>
        </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto h-auto max-height-vh-100 h-100" id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item mb-2 mt-0">
                <a data-bs-toggle="collapse" href="#ProfileNav" class="nav-link text-dark" aria-controls="ProfileNav"
                    role="button" {{ ($menuParent=='user' ) ? ' aria-expanded="true"' : ' aria-expanded="false"' }}>
                    {{-- <img class="rounded-circle" width="32" height="32" src="{{ Auth::user()->profile_photo_url }}"
                    class="avatar" alt="{{ Auth::user()->name }}"> --}}
                    <span class="material-icons-round opacity-10">
                        face
                    </span>
                    <span class="nav-link-text ms-2 ps-1">{{ Auth::user()->name }}</span>
                </a>
                <div class="collapse" id="ProfileNav" style="">
                    <ul class="nav ">
                        <li class="nav-item {{ $activePage == 'user_profile' ? 'active' : ''}}">
                            <a class="nav-link text-dark" href="{{ route('user-profile.show', Auth::user()->id) }}">
                                <i class="material-icons-round opacity-10">account_circle</i>
                                <span class="sidenav-normal  ms-3  ps-1">{{ __('My Profile') }}</span>
                            </a>
                        </li>
                        {{-- @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="{{ route('api-tokens.index') }}">
                        <span class="sidenav-mini-icon">{{ __('API') }}</span>
                        <span class="sidenav-normal  ms-3  ps-1">{{ __('API Tokens') }}</span>
                        </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link text-dark " href="../../pages/pages/account/settings.html">
                    <span class="sidenav-mini-icon"> S </span>
                    <span class="sidenav-normal  ms-3  ps-1"> Settings </span>
                </a>
            </li> --}}
            <li class="nav-item">
                <a class="nav-link text-dark " href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="material-icons-round opacity-10">logout</i>
                    <span class="sidenav-normal  ms-3  ps-1">{{ __('Log out') }}</span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </a>
            </li>
        </ul>
    </div>
    </li>
    <hr class="horizontal dark mt-0">
    <li class="nav-item mt-3">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder text-dark">{{ __('TEAMS') }}</h6>
    </li>
    {{-- Team Management Menus --}}
    {{-- @if (Auth::user()->currentTeam === null)
    <li class="nav-item {{ $activePage == 'dashboard' ? 'active' : '' }}" {{ $activePage=='dashboard'
                    ? "data-color='success'" : '' }}>
    <a class="nav-link {{ $activePage == 'dashboard' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
        {{ $activePage=='dashboard' ? "data-color='success'" : '' }} href="{{route('teams.index')}}">
        <i class="material-icons-round opacity-10">groups</i>
        <span class="nav-link-text ms-2 ps-1">{{ __('No Active Team') }}</span>
    </a>
    </li> --}}
    @if (Auth::user()->teams->count() < 1) <li class="nav-item ">
        <a class="nav-link {{ $activePage == 'teamCreate' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
            href="{{ route('teams.create') }}">
            <i class="material-icons-round opacity-10">group_add</i>
            <span class="nav-link-text ms-2 ps-1">{{ __('Create Team') }}</span>
        </a>
        </li>
        @elseif (Auth::user()->currentTeam !== null)
        <li class="nav-item ">
            <a data-bs-toggle="collapse" href="#TeamNav"
                class="nav-link text-dark {{ ($menuParent == 'team') ? ' active' : '' }}" aria-controls="TeamNav"
                role="button" {{ ($menuParent=='team' ) ? ' aria-expanded="true"'
                        : ' aria-expanded="false"' }}>
                <i class="material-icons-round opacity-10">groups</i>

                <span class="nav-link-text ms-2 ps-1">{{ Auth::user()->currentTeam->name}}</span>
            </a>
            <div class="collapse  {{ $menuParent == 'teams.index' ? 'show' : '' }} " id="TeamNav">
                <ul class="nav ">
                    <li class="nav-item {{ $activePage == 'teamMembers' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'teamMembers' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="{{route('teams.members.show', Auth::user()->currentTeam->id)}}">
                            {{-- <span class="sidenav-mini-icon"> T </span> --}}
                            <i class="material-icons-round opacity-10">people_alt</i>
                            <span class="sidenav-normal  ms-2  ps-1">{{ __('Team Members') }}</span>
                        </a>
                    </li>
                    <li class="nav-item {{ $activePage == 'teamCreate' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'teamCreate' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="{{route('teams.create')}}">
                            {{-- <span class="sidenav-mini-icon"> CT </span> --}}
                            <i class="material-icons-round opacity-10">group_add</i>
                            <span class="sidenav-normal  ms-2  ps-1">{{ __('Create Team') }}</span>
                        </a>
                    </li>
                    <li class="nav-item {{ $activePage == 'teamEdit' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'teamEdit' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="{{route('teams.edit', Auth::user()->currentTeam->id)}}">
                            {{-- <span class="sidenav-mini-icon"> L </span> --}}
                            <i class="material-icons-round opacity-10">auto_fix_high</i>
                            <span class="sidenav-normal  ms-2  ps-1">{{ __('Edit Team') }}</span>
                        </a>
                    </li>
                    {{-- Team Role Setting --}}
                    @can('teams_manage')
                    <li class="nav-item {{ $activePage == 'teamRole' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'teamRole' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="{{route('teams.roles.index', Auth::user()->current_team_id)}}">
                            {{-- <span class="sidenav-mini-icon"> L </span> --}}
                            <i class="material-icons-round opacity-10">manage_accounts</i>
                            <span class="sidenav-normal  ms-2  ps-1">{{ __('Team Roles') }}</span>
                        </a>
                    </li>
                    @can('is super admin')
                    <li class="nav-item {{ $activePage == 'teamPermission' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'teamPermission' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="{{route('teams.edit', Auth::user()->currentTeam->id)}}">
                            {{-- <span class="sidenav-mini-icon"> L </span> --}}
                            <i class="material-icons-round opacity-10">lock</i>
                            <span class="sidenav-normal  ms-2  ps-1">{{ __('Team Permissions') }}</span>
                        </a>
                    </li>
                    @endcan
                    @endcan
                </ul>
            </div>
        </li>
        @endif
        {{-- Switch Team --}}
        @if (Auth::user()->teams->count() > 1 || Auth::user()->currentTeam === null)
        <li class="nav-item mb-2 mt-0">
            <a data-bs-toggle="collapse" href="#SwitchTeamNav"
                class="nav-link {{ ($activePage == 'teamIndex') ? 'text-white bg-gradient-success' : 'text-dark' }}"
                aria-controls="SwitchTeamNav" role="button" {{ ($activePage=='teamIndex' )
                        ? ' aria-expanded="true"' : ' aria-expanded="false"' }}>
                <i class="material-icons-round opacity-10">login</i>
                <span class="nav-link-text ms-2 ps-1">{{ __('Switch Team') }}</span>
            </a>
            <div class="collapse {{ $activePage == 'teamIndex' ? 'show' : '' }} " id="SwitchTeamNav" style="">
                <ul class="nav ">
                    @foreach (Auth::user()->teams as $team)
                    <li class="nav-item dropdown">
                        <a class="nav-link text-dark active" href="{{ route('teams.switch', $team->id) }}">
                            <i class="material-icons-round opacity-10">how_to_reg</i>
                            <span class="sidenav-normal  ms-2  ps-1">{{ $team->name }}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </li>
        @endif
        {{-- @if (Auth::user()->teams->count() > 1)
                <li class="nav-item ">
                    <a data-bs-toggle="collapse" href="#dashboardsExamples" class="nav-link text-dark "
                        aria-controls="dashboardsExamples" role="button">
                        <i class="material-icons-round opacity-10">groups</i>
                        <span class="nav-link-text ms-2 ps-1">{{ __('Switch Team')}}</span>
        </a>
        <div class="collapse  {{ $menuParent == 'team' ? 'show' : '' }} " id="dashboardsExamples">
            @foreach (Auth::user()->teams as $team)
            <a class="dropdown-item" href="{{ route('teams.switch', $team->id) }}">
                {{ $team->name }}
            </a>
            @endforeach
        </div>
        </li>
        @endif --}}
        <hr class="horizontal dark mt-0">
        {{-- Dashboard Menu --}}
        <li class="nav-item {{ $activePage == 'dashboard' ? 'active' : '' }}" {{ $activePage=='dashboard'
                    ? "data-color='success'" : '' }}>
            <a class="nav-link {{ $activePage == 'dashboard' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                {{ $activePage=='dashboard' ? "data-color='success'" : '' }} href="{{route('dashboard')}}">
                <i class="material-icons-round opacity-10">dashboard</i>
                <span class="nav-link-text ms-2 ps-1">{{ __('Dashboard') }}</span>
            </a>
        </li>
        {{-- Farm Menu --}}
        <li class="nav-item ">
            <a data-bs-toggle="collapse" href="#FarmNav" class="nav-link text-dark" aria-controls="FarmNav"
                role="button" {{ ($menuParent=='farm' ) ? ' aria-expanded="true"' : ' aria-expanded="false"' }}>
                <i class="material-icons-round opacity-10">agriculture</i>
                <span class="nav-link-text ms-2 ps-1">{{ __('Farm') }}</span>
            </a>
            <div class="collapse  {{ $menuParent == 'farm' ? 'show' : '' }} " id="FarmNav">
                <ul class="nav ">
                    <li class="nav-item {{ $activePage == 'buck' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'buck' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="#">
                            <span class="sidenav-mini-icon"> BL </span>
                            <span class="sidenav-normal  ms-2  ps-1"> Buck List </span>
                        </a>
                    </li>
                    <li class="nav-item {{ $activePage == 'doe' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'doe' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="#">
                            <span class="sidenav-mini-icon"> DL </span>
                            <span class="sidenav-normal  ms-2  ps-1"> Doe List </span>
                        </a>
                    </li>
                    <li class="nav-item {{ $activePage == 'livestock' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'livestock' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="#">
                            <span class="sidenav-mini-icon"> L </span>
                            <span class="sidenav-normal  ms-2  ps-1"> Livestock </span>
                        </a>
                    </li>
                    <li class="nav-item {{ $activePage == 'breeding' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'breeding' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="#">
                            <span class="sidenav-mini-icon"> B </span>
                            <span class="sidenav-normal  ms-2  ps-1"> Breeding </span>
                        </a>
                    </li>
                    <li class="nav-item {{ $activePage == 'kindlingRecord' ? 'active' : '' }}">
                        <a class="nav-link {{ $activePage == 'kindlingRecord' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                            href="#">
                            <span class="sidenav-mini-icon"> KR </span>
                            <span class="sidenav-normal  ms-2  ps-1"> Kindling Record </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link text-dark" href="#" target="_blank">
                <i
                    class="material-icons-round {% if page.brand == 'RTL' %}ms-2{% else %} me-2{% endif %} opacity-10">receipt_long</i>
                <span class="nav-link-text text-dark ms-2 ps-1">Changelog</span>
            </a>
        </li>
        </ul>
        </div>
</aside>
