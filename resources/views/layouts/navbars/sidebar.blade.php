<aside
    class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 ps ps--active-y bg-white"
    id="sidenav-main" data-color="success">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer opacity-5 position-absolute end-0 top-0 d-xl-none text-dark"
            aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="{{ route('dashboard') }}">
            <img src="/logo-veegro.png" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold text-dark"></span>
        </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto h-auto max-height-vh-100 h-100" id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item mb-2 mt-0">
                <a data-bs-toggle="collapse" href="#ProfileNav" class="nav-link text-dark" aria-controls="ProfileNav"
                    role="button" {{ ($menuParent=='user' ) ? ' aria-expanded="true"' : ' aria-expanded="false"' }}>
                    {{-- <img class="rounded-circle" width="32" height="32" src="{{ Auth::user()->profile_photo_url }}"
                    class="avatar" alt="{{ Auth::user()->name }}"> --}}
                    <span class="material-icons-round opacity-10">
                        face
                    </span>
                    <span class="nav-link-text ms-2 ps-1">{{ Auth::user()->name }}</span>
                </a>
                <div class="collapse" id="ProfileNav" style="">
                    <ul class="nav ">
                        <li class="nav-item {{ $activePage == 'user_profile' ? 'active' : ''}}">
                            <a class="nav-link text-dark" href="{{ route('user-profile.show', Auth::user()->id) }}">
                                <i class="material-icons-round opacity-10">account_circle</i>
                                <span class="sidenav-normal  ms-3  ps-1">{{ __('My Profile') }}</span>
                            </a>
                        </li>
                        {{-- @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="{{ route('api-tokens.index') }}">
                        <span class="sidenav-mini-icon">{{ __('API') }}</span>
                        <span class="sidenav-normal  ms-3  ps-1">{{ __('API Tokens') }}</span>
                        </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link text-dark " href="../../pages/pages/account/settings.html">
                    <span class="sidenav-mini-icon"> S </span>
                    <span class="sidenav-normal  ms-3  ps-1"> Settings </span>
                </a>
            </li> --}}
            <li class="nav-item">
                <a class="nav-link text-dark " href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="material-icons-round opacity-10">logout</i>
                    <span class="sidenav-normal  ms-3  ps-1">{{ __('Log out') }}</span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </a>
            </li>
        </ul>
    </div>
    </li>
    @can('is super admin')
    <hr class="horizontal dark mt-0">
    <li class="nav-item mt-3">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder text-dark">{{ __('ADMIN') }}</h6>
    </li>
    {{-- Admin Menu --}}
    <li class="nav-item {{ $parentPage == 'User' ? 'active' : '' }}" {{ $activePage=='User'
                ? "data-color='success'" : '' }}>
        <a class="nav-link {{ $parentPage== 'User' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
            {{ $parentPage=='User' ? "data-color='success'" : '' }} href="{{route('admin.users.index')}}">
            <i class="material-icons-round opacity-10">supervisor_account</i>
            <span class="nav-link-text ms-2 ps-1">{{ __('User') }}</span>
        </a>
    </li>
    <li class="nav-item {{ $parentPage == 'Plan' ? 'active' : '' }}" {{ $activePage=='Plan'
                ? "data-color='success'" : '' }}>
        <a class="nav-link {{ $parentPage == 'Plan' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
            {{ $parentPage=='Plan' ? "data-color='success'" : '' }} href="{{route('admin.plans.index')}}">
            <i class="material-icons-round opacity-10">price_change</i>
            <span class="nav-link-text ms-2 ps-1">{{ __('Plan') }}</span>
        </a>
    </li>
    <li class="nav-item {{ $parentPage == 'Role' ? 'active' : '' }}" {{ $activePage=='Role'
                ? "data-color='success'" : '' }}>
        <a class="nav-link {{ $parentPage == 'Role' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
            {{ $parentPage=='Role' ? "data-color='success'" : '' }} href="{{route('admin.roles.index')}}">
            <i class="material-icons-round opacity-10">admin_panel_settings</i>
            <span class="nav-link-text ms-2 ps-1">{{ __('Role') }}</span>
        </a>
    </li>
    <li class="nav-item {{ $parentPage == 'Permission' ? 'active' : '' }}" {{ $activePage=='Permission'
                ? "data-color='success'" : '' }}>
        <a class="nav-link {{ $parentPage == 'Permission' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
            {{ $parentPage=='Permission' ? "data-color='success'" : '' }} href="{{route('admin.permissions.index')}}">
            <i class="material-icons-round opacity-10">settings_accessibility</i>
            <span class="nav-link-text ms-2 ps-1">{{ __('Permission') }}</span>
        </a>
    </li>
    @endcan
    <hr class="horizontal dark mt-0">
    {{-- Dashboard Menu --}}
    <li class="nav-item {{ $activePage == 'dashboard' ? 'active' : '' }}" {{ $activePage=='dashboard'
                    ? "data-color='success'" : '' }}>
        <a class="nav-link {{ $activePage == 'dashboard' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
            {{ $activePage=='dashboard' ? "data-color='success'" : '' }} href="{{route('dashboard')}}">
            <i class="material-icons-round opacity-10">dashboard</i>
            <span class="nav-link-text ms-2 ps-1">{{ __('Dashboard') }}</span>
        </a>
    </li>
    {{-- Farm Menu --}}
    <li class="nav-item ">
        <a data-bs-toggle="collapse" href="#FarmNav" class="nav-link text-dark" aria-controls="FarmNav" role="button"
            {{ ($menuParent=='farm' ) ? ' aria-expanded="true"' : ' aria-expanded="false"' }}>
            <i class="material-icons-round opacity-10">agriculture</i>
            <span class="nav-link-text ms-2 ps-1">{{ __('Farm') }}</span>
        </a>
        <div class="collapse  {{ $menuParent == 'farm' ? 'show' : '' }} " id="FarmNav">
            <ul class="nav ">
                <li class="nav-item {{ $activePage == 'buck' ? 'active' : '' }}">
                    <a class="nav-link {{ $activePage == 'buck' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                        href="#">
                        <span class="sidenav-mini-icon"> BL </span>
                        <span class="sidenav-normal  ms-2  ps-1"> Buck List </span>
                    </a>
                </li>
                <li class="nav-item {{ $activePage == 'doe' ? 'active' : '' }}">
                    <a class="nav-link {{ $activePage == 'doe' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                        href="#">
                        <span class="sidenav-mini-icon"> DL </span>
                        <span class="sidenav-normal  ms-2  ps-1"> Doe List </span>
                    </a>
                </li>
                <li class="nav-item {{ $activePage == 'livestock' ? 'active' : '' }}">
                    <a class="nav-link {{ $activePage == 'livestock' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                        href="#">
                        <span class="sidenav-mini-icon"> L </span>
                        <span class="sidenav-normal  ms-2  ps-1"> Livestock </span>
                    </a>
                </li>
                <li class="nav-item {{ $activePage == 'breeding' ? 'active' : '' }}">
                    <a class="nav-link {{ $activePage == 'breeding' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                        href="#">
                        <span class="sidenav-mini-icon"> B </span>
                        <span class="sidenav-normal  ms-2  ps-1"> Breeding </span>
                    </a>
                </li>
                <li class="nav-item {{ $activePage == 'kindlingRecord' ? 'active' : '' }}">
                    <a class="nav-link {{ $activePage == 'kindlingRecord' ? 'text-white bg-gradient-success active' : 'text-dark' }}"
                        href="#">
                        <span class="sidenav-mini-icon"> KR </span>
                        <span class="sidenav-normal  ms-2  ps-1"> Kindling Record </span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link text-dark" href="#" target="_blank">
            <i
                class="material-icons-round {% if page.brand == 'RTL' %}ms-2{% else %} me-2{% endif %} opacity-10">receipt_long</i>
            <span class="nav-link-text text-dark ms-2 ps-1">Changelog</span>
        </a>
    </li>
    </ul>
    </div>
</aside>