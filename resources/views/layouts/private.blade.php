<!--
=========================================================
* Material Dashboard PRO - v3.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="/material/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="/VG.png">
    <title>
        Dashboard - Veegro
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <!-- Nucleo Icons -->
    <link href="/material/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/material/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    <link id="pagestyle" href="/material/assets/css/material-dashboard.css" rel="stylesheet" />

</head>

<body class="g-sidenav-show bg-gray-200">
    @include('layouts.navbars.sidebar')

    <div class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        @include('layouts.navbars.navbar')
        @yield('content')
        {{-- @include('layouts.footers.auth') --}}
    </div>


    <!--   Core JS Files   -->
    <script src="/material/assets/js/core/popper.min.js"></script>
    <script src="/material/assets/js/core/bootstrap.min.js"></script>
    <script src="/material/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="/material/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script src="/material/assets/js/plugins/datatables.js"></script>
    <script src="/material/assets/js/plugins/flatpickr.min.js"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    {{-- <script src="/material/assets/js/plugins/jasny-bootstrap.min.js"></script> --}}
    <!-- Plugin for the charts, full documentation here: https://www.chartjs.org/ -->
    <script src="/material/assets/js/plugins/chartjs.min.js"></script>
    {{-- <script src="/material/assets/js/plugins/Chart.extension.js"></script> --}}
    <!-- Kanban scripts -->
    <script src="/material/assets/js/plugins/dragula/dragula.min.js"></script>
    <script src="/material/assets/js/plugins/jkanban/jkanban.js"></script>
    @includeWhen(Auth::user(),'layouts.footers.authscript')
    @include('layouts.footers.alertscript')
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="/material/assets/js/material-dashboard.min.js?v=3.0.1"></script>
</body>

</html>
