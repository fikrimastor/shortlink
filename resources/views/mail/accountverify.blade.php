@extends('mail.layout', [
'mailPurpose' => 'Email Verification'
])

@section('content')

<h1>{{__('We need to verify your email')}}</h1>

{{$name}},

<p>Your registration at {{config('app.name')}} is pending an email verification, please click the link below to verify
    your account.</p>

<p> <a href="{{ route('login')}}">Login To Veegro</a></p>

<p>If you have any problems, please feel free to reply to this email to get in touch with us!</p>


<p>Veegro Registration System</p>

@endsection
