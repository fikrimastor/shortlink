@extends('mail.layout',[
'mailPurpose' => 'Subscription Reminder'
])

@section('content')

<h1>{{__('Your account will expire')}}</h1>

{{$name}},

<p>Your account in {{config('app.name')}} will expire soon. You can renew your subscription via this page:</p>

<p>Expiry Date: {{$expire_at}}</p>

<p> <a href="{{ route('signup')}}">Renew Your Subscription Now</a></p>

<p>Veegro Registration System</p>

@endsection
