@extends('mail.layout', [
'mailPurpose' => 'Received Payment'
])

@section('content')

<h1>{{__('Thank you for your payment')}}</h1>

{{$name}},

<p>Your account in {{config('app.name')}} will expire soon. You can renew your subscription via this page:</p>
<p>We have receive your payment {{ $amount }}</p>

<p>Your subscription now renewed until {{$expire_at}}</p>

<p>Login to the dashboard to check your current subscription status:</p>

<p> <a href="{{ route('login')}}">Login To Veegro</a></p>

<p>Veegro Registration System</p>

@endsection
