@extends('layouts.private',[
'activePage' => 'teamIndex',
'parentPage' => '',
'menuParent' => '',
'title' => __('Teams')
])
@section('content')
{{-- <div class="container-fluid py-4">
    <div class="row mt-4">
        <div class="col-12">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            <h5 class="mb-0">{{__('Teams')}}</h5>
                            <p class="text-sm mb-0">
                                List of your team.
                            </p>
                        </div>
                        <div class="col-6 text-end">
                            <a href="{{route('teams.create')}}" class="btn bg-gradient-success mb-0">
                                <i class="material-icons text-sm">add</i>&nbsp;&nbsp;{{__('Create Team')}}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    {{__('Name')}}</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    {{__('Status')}}</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    {{__('Members')}}</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    {{__('Switch')}}</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($teams as $team)
                            <tr>
                                <td class="text-sm font-weight-normal">{{$team->name}}
                                </td>
                                <td class="text-sm font-weight-normal">
                                    @if(auth()->user()->isOwnerOfTeam($team))
                                    <span class="label label-success">Owner</span>
                                    @else
                                    <span class="label label-primary">Member</span>
                                    @endif
                                </td>
                                <td class="text-sm font-weight-normal">
                                    <a href="{{route('teams.members.show', $team)}}" class="btn btn-sm btn-default">
                                        <i class="fa fa-users"></i> Members
                                    </a>
                                </td>
                                <td class="text-sm font-weight-normal">
                                    @if(is_null(auth()->user()->currentTeam) || auth()->user()->currentTeam->getKey()
                                    !== $team->getKey())
                                    <a href="{{route('teams.switch', $team)}}" class="btn btn-sm btn-default">
                                        <i class="fa fa-sign-in"></i> Switch
                                    </a>
                                    @else
                                    <span class="label label-default">Current team</span>
                                    @endif
                                </td>
                                <td class="text-sm font-weight-normal">
                                    @if(auth()->user()->isOwnerOfTeam($team))

                                    <a href="{{route('teams.edit', $team)}}" class="btn btn-sm btn-default">
                                        <button class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i>
                                            Edit</button>
                                    </a>

                                    <form style="display: inline-block;" action="{{route('teams.destroy', $team)}}"
                                        method="post" class="btn btn-sm btn-default">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i>
                                            Delete</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footers.auth')
</div> --}}
<div class="container-fluid py-4">
    <section class="py-3">
        @if( session('success') )
        @include('layouts.alerts.feedback')
        @endif
        <div class="row mb-4 mb-md-0">
            <div class="col-md-8 me-auto my-auto text-left">
                <h5>List of All Your Teams</h5>
                <p>You can see all your teams here. <br> You may edit, view or switch team from this page.</p>

            </div>
            <div class="col-lg-4 col-md-12 my-auto text-end">
                {{-- <button type="button" class="btn bg-gradient-primary mb-0 mt-0 mt-md-n9 mt-lg-0"
                    href="{{route('teams.create')}}">

                </button> --}}
                <a href="{{route('teams.create')}}" class="btn bg-gradient-warning mb-0 mt-0 mt-md-n9 mt-lg-0">
                    <i class="material-icons text-white position-relative text-md pe-2">add</i>{{__('Create Team')}}
                </a>
            </div>
        </div>

        <div class="row mt-lg-4 mt-2">
            @foreach ($teams as $team)
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card">
                    <div class="card-body p-3">
                        <div class="d-flex mt-n2">
                            {{-- <div class="avatar avatar-xl bg-gradient-dark border-radius-xl p-2 mt-n4">
                                <img src="../../../assets/img/small-logos/logo-slack.svg" alt="slack_logo">
                            </div> --}}
                            <h6 class="mb-0">{{$team->name}}</h6>
                            {{-- <div class="ms-3 my-auto">
                                <div class="avatar-group">
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Jessica Rowland">
                                        <img alt="Image placeholder" src="../../../assets/img/team-3.jpg" class="">
                                    </a>
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Audrey Love">
                                        <img alt="Image placeholder" src="../../../assets/img/team-4.jpg"
                                            class="rounded-circle">
                                    </a>
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Michael Lewis">
                                        <img alt="Image placeholder" src="../../../assets/img/team-2.jpg"
                                            class="rounded-circle">
                                    </a>
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Jessica Rowland">
                                        <img alt="Image placeholder" src="../../../assets/img/team-3.jpg" class="">
                                    </a>
                                    <a href="javascript:;" class="avatar avatar-xs rounded-circle"
                                        data-bs-toggle="tooltip" data-original-title="Audrey Love">
                                        <img alt="Image placeholder" src="../../../assets/img/team-4.jpg"
                                            class="rounded-circle">
                                    </a>
                                </div>
                            </div> --}}
                            <div class="ms-auto">
                                <div class="dropdown">
                                    <button class="btn btn-link text-secondary ps-0 pe-2" id="navbarDropdownMenuLink"
                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v text-lg"></i>
                                    </button>
                                    @if(auth()->user()->isOwnerOfTeam($team))
                                    <div class="dropdown-menu dropdown-menu-end me-sm-n4 me-n3"
                                        aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="{{route('teams.edit', $team)}}"><i
                                                class="fa fa-pencil"></i> Edit</a>
                                        {{-- <a class="dropdown-item" href="{{route('teams.destroy', $team)}}">
                                            <i class="fa fa-trash-o"></i>{{__('Delete')}}
                                            <form style="display: inline-block;"
                                                action="{{route('teams.destroy', $team)}}" method="post"
                                                class="btn btn-sm btn-default">
                                                @csrf
                                                <input type="hidden" name="_method" value="DELETE" />
                                            </form> --}}
                                            {{-- TODO:Make a modal --}}
                                            <form style="display: inline-block;"
                                                action="{{route('teams.destroy', $team)}}" method="post">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i>
                                                    Delete</button>
                                                <a class="dropdown-item"
                                                    onclick="material.showSwal('warning-message-and-confirmation')"><i
                                                        class="fa fa-trash-o"></i>{{__('Delete')}}
                                            </form>
                                        </a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <p class="text-sm mt-3"> Location:
                            {{$team->profile->city}}
                        </p>
                        <p class="text-sm mt-3"> Description:
                            {{$team->profile->description}}
                        </p>
                        <hr class="horizontal dark">
                        <div class="row">
                            <div class="col-6">
                                <h6 class="text-sm mb-0">
                                    @if(auth()->user()->isOwnerOfTeam($team))
                                    <span class="label label-success">Owner</span>
                                    @else
                                    <span class="label label-primary">Member</span>
                                    @endif
                                </h6>
                                <p class="text-secondary text-sm font-weight-normal mb-0">Status</p>
                            </div>
                            <div class="col-6 text-end">
                                {{-- <h6 class="text-sm mb-0">02.03.22</h6> --}}
                                {{-- <p class="text-secondary text-sm font-weight-normal mb-0">Due date</p> --}}
                                @if(is_null(auth()->user()->currentTeam) || auth()->user()->currentTeam->getKey() !==
                                $team->getKey())
                                <a href="{{route('teams.switch', $team)}}" class="btn btn-sm bg-gradient-secondary">
                                    <i class="fa fa-sign-in"></i> Switch Team
                                </a>
                                @else
                                <span class="label label-default">Current team</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100 ">
                    <div class="card-body d-flex flex-column justify-content-center text-center">
                        <a href="{{route('teams.create')}}" class="mb-1 mt-1 mt-md-n9 mt-lg-0 text-center">
                            <i class="material-icons text-secondary">group_add</i>
                            <h5 class="text-secondary">
                                {{__('Create Team')}}
                            </h5>
                        </a>
                        <br />
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-lg-4 mt-2">

        </div>
    </section>
    @include('layouts.footers.auth')
</div>
@endsection
