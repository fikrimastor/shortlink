@extends('layouts.private',[
'activePage' => 'dashboard',
'parentPage' => '',
'menuParent' => 'dashboard',
'title' => __('Dashboard')
])

@section('content')
<div class="container-fluid my-3 py-3">
    @if( session('success') )
    @include('layouts.alerts.feedback')
    @endif
    <div class="row mb-5">
        <div class="col-lg-3">
            <div class="card position-sticky top-1">
                <ul class="nav flex-column bg-white border-radius-lg p-3">
                    <li class="nav-item">
                        <a class="nav-link text-dark d-flex" data-scroll="" href="#profile">
                            <i class="material-icons text-lg me-2">person</i>
                            <span class="text-sm">{{__('Profile')}}</span>
                        </a>
                    </li>
                    <li class="nav-item pt-2">
                        <a class="nav-link text-dark d-flex" data-scroll="" href="#basic-info">
                            <i class="material-icons text-lg me-2">receipt_long</i>
                            <span class="text-sm">{{__('Basic Info')}}</span>
                        </a>
                    </li>
                    <li class="nav-item pt-2">
                        <a class="nav-link text-dark d-flex" data-scroll="" href="#password">
                            <i class="material-icons text-lg me-2">lock</i>
                            <span class="text-sm">{{__('Change Password')}}</span>
                        </a>
                    </li>
                    <li class="nav-item pt-2">
                        <a class="nav-link text-dark d-flex" data-scroll="" href="#2fa">
                            <i class="material-icons text-lg me-2">security</i>
                            <span class="text-sm">{{__('2FA')}}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-9 mt-lg-0 mt-4">
            <!-- Card Profile -->
            <div class="card card-body" id="profile">
                <div class="row justify-content-center align-items-center">
                    <div class="col-sm-auto col-4">
                        {{-- <div class="avatar avatar-xl position-relative">
                            <img src="../../../assets/img/bruce-mars.jpg" alt="bruce"
                                class="w-100 rounded-circle shadow-sm">
                        </div> --}}
                    </div>
                    <div class="col-sm-auto col-8 my-auto">
                        <div class="h-100">
                            <h5 class="mb-1 font-weight-bolder">
                                {{Auth::user()->name}}
                            </h5>
                            <p class="mb-0 font-weight-normal text-sm">
                                {{__('CEO / Co-Founder')}}
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-auto ms-sm-auto mt-sm-0 mt-3 d-flex">
                        <label class="form-check-label mb-0">
                            <small id="profileVisibility">
                                {{__('Switch to invisible')}}
                            </small>
                        </label>
                        <div class="form-check form-switch ms-2 my-auto">
                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault23" checked
                                onchange="visible()">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card Basic Info -->
            <div class="card mt-4" id="basic-info">
                <div class="card-header">
                    <h5>{{__('Basic Info')}}</h5>
                </div>
                <div class="card-body pt-0">
                    <form class="form-horizontal" method="POST" action="{{route('user-profile.update', $user)}}">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>{{__('First Name')}}</label>
                                    <input type="text" class="form-control" name="firstname"
                                        value="{{old('firstname', $user->profile->firstname)}}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>{{__('Last Name')}}</label>
                                    <input type="text" class="form-control" name="lastname"
                                        value="{{old('lastname', $user->profile->lastname)}}">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-6">
                                <label class="form-label ms-0">{{__("I'm")}}</label>
                                <select class="form-control" name="gender" id="choices-gender">
                                    <option value="1" {{old('gender', $user->profile->gender) == 1 ? 'selected' : '' }}>
                                        Male
                                    </option>
                                    <option value="2" {{old('gender', $user->profile->gender) == 2 ? 'selected' : '' }}>
                                        Female</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <label>{{__('Birth Date')}}</label>
                                <div
                                    class="input-group input-group-static @error('name') has-error text-danger @enderror">
                                    <input class="form-control datepicker" name="dob"
                                        placeholder="{{old('dob', $user->profile->dob) ? $user->profile->dob : 'Select date of birth' }}"
                                        type="date">
                                    @error('dob')
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>{{__('Email')}}</label>
                                    <input type="email" class="form-control"
                                        placeholder="{{old('email', $user->email)}}"
                                        value="{{old('email', $user->email)}}" name="email">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>{{__('Confirm Email')}}</label>
                                    <input type="email" class="form-control"
                                        placeholder="{{old('email', $user->email)}}"
                                        value="{{old('email', $user->email)}}" name="email_confirmation">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>{{__('Your location (City)')}}</label>
                                    <input type="text" class="form-control"
                                        placeholder="{{old('gender', $user->profile->city)? $user->profile->city : 'Your nearest city' }}"
                                        value="{{old('city', $user->profile->city)}}" name="city">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group input-group-static">
                                    <label>{{__('Phone Number')}}</label>
                                    <input type="number" class="form-control"
                                        placeholder="{{old('phone', $user->profile->phone) ? $user->profile->phone : 'Your phone number' }}"
                                        value="{{old('phone', $user->profile->phone)}}" name="phone">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn bg-gradient-dark btn-sm float-end mt-6 mb-0">{{__('Update
                            profile')}}</button>
                    </form>
                </div>
            </div>
            <!-- Card Change Password -->
            <div class="card mt-4" id="password">
                <div class="card-header">
                    <h5>{{__('Change Password')}}</h5>
                </div>
                <div class="card-body pt-0">
                    <form action="{{ route('user-password.update') }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="input-group input-group-outline">
                            <label class="form-label">{{__('Current password')}}</label>
                            <input type="password" class="form-control" name="current_password" required
                                autocomplete="current-password">
                        </div>
                        <div class="input-group input-group-outline my-4">
                            <label class="form-label">{{__('New password')}}</label>
                            <input type="password" class="form-control" name="password" required
                                autocomplete="new-password">
                        </div>
                        <div class="input-group input-group-outline">
                            <label class="form-label">{{__('Confirm New password')}}</label>
                            <input type="password" class="form-control" name="password_confirmation" required
                                autocomplete="new-password">
                        </div>
                        <h5 class="mt-5">{{__('Password requirements')}}</h5>
                        <p class="text-muted mb-2">
                            {{__('Please follow this guide for a strong password:')}}
                        </p>
                        <ul class="text-muted ps-4 mb-0 float-start">
                            <li>
                                <span class="text-sm">{{__('One special characters')}}</span>
                            </li>
                            <li>
                                <span class="text-sm">{{__('Min 6 characters')}}</span>
                            </li>
                            <li>
                                <span class="text-sm">{{__('One number (2 are recommended)')}}</span>
                            </li>
                            <li>
                                <span class="text-sm">{{__('Change it often')}}</span>
                            </li>
                        </ul>
                        <button type="submit" class="btn bg-gradient-dark btn-sm float-end mt-6 mb-0">{{__('Update
                            password')}}</button>
                    </form>
                </div>
            </div>
            <!-- Card 2FA Password -->
            <div class="card mt-4" id="2fa">
                <div class="card-header d-flex">
                    <h5 class="mb-0">{{__('Two-factor authentication')}}</h5>
                    @if(session('status') == 'two-factor-authentication-enabled')
                    <span class="badge badge-success ms-auto mb-auto">{{__('Enabled')}}</span>
                    @else
                    <span class="badge badge-secondary ms-auto mb-auto">{{__('Disabled')}}</span>
                    @endif
                </div>
                <div class="card-body">
                    <div class="d-flex">
                        <p class="my-auto">{{__('Status')}}</p>
                        {{-- <p class="text-secondary text-sm ms-auto my-auto me-3">{{__('Not Configured')}}</p> --}}
                        {{-- <button class="btn btn-sm btn-outline-dark mb-0" type="button">{{__('Add')}}</button> --}}
                        @if(! auth()->user()->two_factor_secret)
                        {{-- Enable 2FA --}}
                        <p class="text-secondary text-sm ms-auto my-auto me-3">{{__('Not Configured')}}</p>
                        {{-- <form method="POST" action="{{ url('user/two-factor-authentication') }}"> --}}
                            <form role="form">
                                @csrf
                                <button class="btn btn-sm btn-outline-dark mb-0" type="submit">{{ __('Enable
                                    Two-Factor')
                                    }}</button>
                            </form>
                            @else
                            {{-- Disable 2FA --}}
                            <p class="text-secondary text-sm ms-auto my-auto me-3">{{__('Configured')}}</p>
                            <form method="POST" action="{{ url('user/two-factor-authentication') }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger mb-0" type="submit">{{ __('Disable Two-Factor')
                                    }}</button>
                            </form>
                            @if(session('status') == 'two-factor-authentication-enabled')
                            {{-- Show SVG QR Code, After Enabling 2FA --}}
                            <p class="text-secondary text-sm ms-auto my-auto me-3">
                                {{ __('Two factor authentication is now enabled. Scan the following QR code using your
                                phone\'s authenticator application.') }}
                            </p>
                            <div>
                                {!! auth()->user()->twoFactorQrCodeSvg() !!}
                            </div>
                            @endif
                    </div>
                    <hr class="horizontal dark">
                    <div class="d-flex">
                        <p class="my-auto">{{__('Security keys')}}</p>
                        @if(session('status') == 'two-factor-authentication-enabled')
                        <p class="text-secondary text-sm ms-auto my-auto me-3">No Security Keys</p>
                        <button class="btn btn-sm btn-outline-dark mb-0" type="button">{{__('Add')}}</button>
                        {{-- Show 2FA Recovery Codes --}}
                        <div>
                            {{ __('Store these recovery codes in a secure password manager. They can be used to recover
                            access to your account if your two factor authentication device is lost.') }}
                        </div>

                        <div>
                            @foreach (json_decode(decrypt(auth()->user()->two_factor_recovery_codes), true) as $code)
                            <div>{{ $code }}</div>
                            @endforeach
                        </div>
                        @else
                        <p class="text-secondary text-sm ms-auto my-auto me-3">No Security Keys</p>
                        @endif
                    </div>
                    <hr class="horizontal dark">
                    <div class="d-flex">
                        <p class="my-auto">{{__('Authenticator app')}}</p>
                        <p class="text-secondary text-sm ms-auto my-auto me-3">Not Configured</p>
                        <button class="btn btn-sm btn-outline-dark mb-0" type="button">{{__('Set up')}}</button>
                        {{-- Regenerate 2FA Recovery Codes --}}
                        <form method="POST" action="{{ url('user/two-factor-recovery-codes') }}">
                            @csrf

                            <button type="submit">
                                {{ __('Regenerate Recovery Codes') }}
                            </button>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Card Delete Account -->
            {{-- <div class="card mt-4" id="delete">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-sm-0 mb-4">
                        <div class="w-50">
                            <h5>{{__('Delete Account')}}</h5>
                            <p class="text-sm mb-0">{{__('Once you delete your account, there is no going back. Please
                                be
                                certain.')}}</p>
                        </div>
                        <div class="w-50 text-end">
                            <button class="btn btn-outline-secondary mb-3 mb-md-0 ms-auto" type="button"
                                name="button">{{__('Deactivate')}}</button>
                            <button class="btn bg-gradient-danger mb-0 ms-2" type="button" name="button">{{__('Delete
                                Account')}}</button>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
    @include('layouts.footers.auth')
</div>
@endsection
