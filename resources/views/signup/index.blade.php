@extends('layouts.public',[
'activePage' => 'signup',
'parentPage' => '',
'menuParent' => '',
'title' => __('Signup')
])
@section('content')

<div class="page-header position-relative m-3 pb-4">
    <div class="container py-6 postion-relative z-index-2">
        <div class="row">
            <div class="col-md-6 mx-auto text-center">
                <img src="/logo-veegro.png" class="my-4" style="width: 150px" alt="Veegro">
                <h2 class="text-dark">Pick the best plan for you</h2>
                <p class="text-dark">You have Free Unlimited Updates and Premium Support on each package.</p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid px-5 my-6 bottom-10">
    <div class="mt-n8">
        <div class="container">
            <div class="tab-content tab-space">
                <div class="tab-pane active" id="price">
                    <div class="row">
                        @foreach ($plans as $plan)
                        <div class="col-lg-4 mb-lg-0 mb-4">
                            <div class="card bg-gradient-dark shadow-lg">
                                <span class="badge rounded-pill bg-warning w-30 mt-n2 mx-auto">{{$plan->name}}</span>
                                <div class="card-header text-center pt-4 pb-3 bg-transparent">
                                    <h1 class="font-weight-bold mt-2 text-white">
                                        <small class="text-lg align-top me-1">RM</small>{{$plan->plan_price}}
                                    </h1>
                                </div>
                                <div class="card-body text-lg-start text-center pt-0">
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto text-white">done</i>
                                        <span class="ps-3 text-white">{{$plan->name == 'Free' ? "Duration:
                                            Lifetime":"Duration: $plan->duration"}}
                                        </span>
                                    </div>
                                    {{-- <a href="javascript:;"
                                        class="plan-btn btn btn-icon bg-gradient-warning d-lg-block mt-3 mb-0"
                                        data-plan-id="{{$plan->id}}" data-bs-toggle="modal"
                                        data-bs-target="#modal-loginOrRegister">
                                        {{__('Select Plan')}}
                                        <i class="fas fa-arrow-right ms-1"></i>
                                    </a> --}}
                                    <button id="plan"
                                        class="plan-btn btn btn-icon bg-gradient-warning d-lg-block mt-3 mb-0"
                                        data-plan-id="{{$plan->id}}" data-bs-toggle="modal"
                                        data-login-status="{{ Auth::check() }}"
                                        data-bs-target="#modal-loginOrRegister">{{__('Select Plan')}}<i
                                            class="fas fa-arrow-right ms-1"></i></button>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-lg-4 mb-lg-0 mb-4">
                            <div class="card shadow-lg">
                                <span class="badge rounded-pill bg-light text-dark w-30 mt-n2 mx-auto">Enterprise</span>
                                <div class="card-header text-center pt-4 pb-3">
                                    <h1 class="font-weight-bold mt-2">
                                        <small class="text-lg align-top me-1">$</small>99<small
                                            class="text-lg">/mo</small>
                                    </h1>
                                </div>
                                <div class="card-body text-lg-start text-center pt-0">
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">Unlimited team members</span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">100GB Cloud storage </span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">Integration help </span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">Sketch Files </span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">API Access </span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">Complete documentation </span>
                                    </div>
                                    <a href="javascript:;" class="btn btn-icon bg-gradient-dark d-lg-block mt-3 mb-0">
                                        Join
                                        <i class="fas fa-arrow-right ms-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div> --}}
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row mt-5">
            <div class="col-8 mx-auto text-center">
                <h6 class="opacity-5"> More than 50+ brands trust Material</h6>
            </div>
        </div> --}}
        {{-- <div class="row mt-4 mx-5">
            <div class="col-lg-2 col-md-4 col-6 mb-4">
                <img class="w-100 opacity-9" src="../../assets/img/logos/gray-logos/logo-coinbase.svg"
                    alt="logo_coinbase">
            </div>
            <div class="col-lg-2 col-md-4 col-6 mb-4">
                <img class="w-100 opacity-9" src="../../assets/img/logos/gray-logos/logo-nasa.svg" alt="logo_nasa">
            </div>
            <div class="col-lg-2 col-md-4 col-6 mb-4">
                <img class="w-100 opacity-9" src="../../assets/img/logos/gray-logos/logo-netflix.svg"
                    alt="logo_netflix">
            </div>
            <div class="col-lg-2 col-md-4 col-6 mb-4">
                <img class="w-100 opacity-9" src="../../assets/img/logos/gray-logos/logo-pinterest.svg"
                    alt="logo_pinterest">
            </div>
            <div class="col-lg-2 col-md-4 col-6 mb-4">
                <img class="w-100 opacity-9" src="../../assets/img/logos/gray-logos/logo-spotify.svg"
                    alt="logo_spotify">
            </div>
            <div class="col-lg-2 col-md-4 col-6 mb-4">
                <img class="w-100 opacity-9" src="../../assets/img/logos/gray-logos/logo-vodafone.svg"
                    alt="logo_vodafone">
            </div>
        </div> --}}
        {{-- <div class="row mt-5">
            <div class="col-md-6 mx-auto text-center">
                <h2>Frequently Asked Questions</h2>
                <p>A lot of people don&#39;t appreciate the moment until it’s passed. I&#39;m not trying my hardest, and
                    I&#39;m not trying to do </p>
            </div>
        </div> --}}
        {{-- <div class="row mb-5">
            <div class="col-md-8 mx-auto">
                <div class="accordion" id="accordionRental">
                    <div class="accordion-item my-2">
                        <h5 class="accordion-header" id="headingOne">
                            <button class="accordion-button border-bottom font-weight-bold" type="button"
                                data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
                                aria-controls="collapseOne">
                                How do I order?
                                <i
                                    class="collapse-close material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">add</i>
                                <i
                                    class="collapse-open material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">remove</i>
                            </button>
                        </h5>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                            data-bs-parent="#accordionRental">
                            <div class="accordion-body text-sm opacity-8">
                                We’re not always in the position that we want to be at. We’re constantly growing. We’re
                                constantly making mistakes. We’re constantly trying to express ourselves and actualize
                                our dreams. If you have the opportunity to play this game
                                of life you need to appreciate every moment. A lot of people don’t appreciate the moment
                                until it’s passed.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item my-2">
                        <h5 class="accordion-header" id="headingTwo">
                            <button class="accordion-button border-bottom font-weight-bold" type="button"
                                data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false"
                                aria-controls="collapseTwo">
                                How can i make the payment?
                                <i
                                    class="collapse-close material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">add</i>
                                <i
                                    class="collapse-open material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">remove</i>
                            </button>
                        </h5>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                            data-bs-parent="#accordionRental">
                            <div class="accordion-body text-sm opacity-8">
                                It really matters and then like it really doesn’t matter. What matters is the people who
                                are sparked by it. And the people who are like offended by it, it doesn’t matter.
                                Because it&#39;s about motivating the doers. Because I’m here to follow my dreams and
                                inspire other people to follow their dreams, too.
                                <br>
                                We’re not always in the position that we want to be at. We’re constantly growing. We’re
                                constantly making mistakes. We’re constantly trying to express ourselves and actualize
                                our dreams. If you have the opportunity to play this game of life you need to appreciate
                                every moment. A lot of people don’t appreciate the moment until it’s passed.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item my-2">
                        <h5 class="accordion-header" id="headingThree">
                            <button class="accordion-button border-bottom font-weight-bold" type="button"
                                data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false"
                                aria-controls="collapseThree">
                                How much time does it take to receive the order?
                                <i
                                    class="collapse-close material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">add</i>
                                <i
                                    class="collapse-open material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">remove</i>
                            </button>
                        </h5>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                            data-bs-parent="#accordionRental">
                            <div class="accordion-body text-sm opacity-8">
                                The time is now for it to be okay to be great. People in this world shun people for
                                being great. For being a bright color. For standing out. But the time is now to be okay
                                to be the greatest you. Would you believe in what you believe in, if you were the only
                                one who believed it?
                                If everything I did failed - which it doesn&#39;t, it actually succeeds - just the fact
                                that I&#39;m willing to fail is an inspiration. People are so scared to lose that they
                                don&#39;t even try. Like, one thing people can&#39;t say is that I&#39;m not trying, and
                                I&#39;m not trying my hardest, and I&#39;m not trying to do the best way I know how.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item my-2">
                        <h5 class="accordion-header" id="headingFour">
                            <button class="accordion-button border-bottom font-weight-bold" type="button"
                                data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false"
                                aria-controls="collapseFour">
                                Can I resell the products?
                                <i
                                    class="collapse-close material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">add</i>
                                <i
                                    class="collapse-open material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">remove</i>
                            </button>
                        </h5>
                        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour"
                            data-bs-parent="#accordionRental">
                            <div class="accordion-body text-sm opacity-8">
                                I always felt like I could do anything. That’s the main thing people are controlled by!
                                Thoughts- their perception of themselves! They&#39;re slowed down by their perception of
                                themselves. If you&#39;re taught you can’t do anything, you won’t do anything. I was
                                taught I could do everything.
                                <br><br>
                                If everything I did failed - which it doesn&#39;t, it actually succeeds - just the fact
                                that I&#39;m willing to fail is an inspiration. People are so scared to lose that they
                                don&#39;t even try. Like, one thing people can&#39;t say is that I&#39;m not trying, and
                                I&#39;m not trying my hardest, and I&#39;m not trying to do the best way I know how.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item my-2">
                        <h5 class="accordion-header" id="headingFifth">
                            <button class="accordion-button border-bottom font-weight-bold" type="button"
                                data-bs-toggle="collapse" data-bs-target="#collapseFifth" aria-expanded="false"
                                aria-controls="collapseFifth">
                                Where do I find the shipping details?
                                <i
                                    class="collapse-close material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">add</i>
                                <i
                                    class="collapse-open material-icons text-sm font-weight-bold pt-1 position-absolute end-0 me-3">remove</i>
                            </button>
                        </h5>
                        <div id="collapseFifth" class="accordion-collapse collapse" aria-labelledby="headingFifth"
                            data-bs-parent="#accordionRental">
                            <div class="accordion-body text-sm opacity-8">
                                There’s nothing I really wanted to do in life that I wasn’t able to get good at. That’s
                                my skill. I’m not really specifically talented at anything except for the ability to
                                learn. That’s what I do. That’s what I’m here for. Don’t be afraid to be wrong because
                                you can’t learn anything from a compliment.
                                I always felt like I could do anything. That’s the main thing people are controlled by!
                                Thoughts- their perception of themselves! They&#39;re slowed down by their perception of
                                themselves. If you&#39;re taught you can’t do anything, you won’t do anything. I was
                                taught I could do everything.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>

<div class="modal fade" id="modal-loginOrRegister" tabindex="-1" role="dialog" aria-labelledby="modal-loginOrRegister"
    aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title font-weight-normal" id="modal-title-signup">{{__('Please login or register')}}
                </h6>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="py-3 text-center">
                    {{-- <h5 class="text-gradient text-danger mt-4"></h5> --}}
                    <p>{{__('We need to confirm your account before proceed into any payment plan.')}}</p>
                </div>
                <!--- Navigation Tab --->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
                            data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                            aria-selected="true"><span class="material-icons align-middle mb-1">
                                badge
                            </span> {{__('Login')}}</button>
                        <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile"
                            type="button" role="tab" aria-controls="nav-profile" aria-selected="false"><span
                                class="material-icons align-middle mb-1">
                                laptop
                            </span>
                            {{__('Register')}}</button>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <!--- Login Form --->
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <form role="form" id="login-form-vg" class="text-start" method="POST"
                            action="{{ route('login') }}">
                            @csrf
                            <div class="alert alert-danger d-none text-white mt-2" id="login-alert">
                                {{__('You login credentials is not valid.')}}
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                                <div class="input-group input-group-static mb-4 mt-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input id="login-form-email"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        placeholder="{{ __('Email') }}" type="email" name="email"
                                        value="{{ old('email') }}" required autofocus>
                                </div>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-static mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input id="login-form-password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        name="password" placeholder="{{ __('Password') }}" type="password"
                                        value="{{ old('password') }}" required>
                                </div>
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="text-center">
                                <button type="button" id="login-button"
                                    class="btn bg-gradient-success w-100 mt-3 mb-0">Sign in</button>
                            </div>
                        </form>
                    </div>
                    <!--- Register Form --->
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <form method="POST" action="{{ route('register') }}" id="register-form-vg">
                            @csrf
                            <div class="alert alert-danger d-none text-white mt-2" id="register-alert">
                                {{__('You details is not valid or the email is already registered.')}}
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-static mb-4 mt-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                                    </div>
                                    <input id="register-form-name"
                                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                        placeholder="{{ __('Name') }}" type="text" name="name" value="{{ old('name') }}"
                                        required autofocus>
                                </div>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                                <div class="input-group input-group-static mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input id="register-form-email"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        placeholder="{{ __('Email') }}" type="email" name="email"
                                        value="{{ old('email') }}" required autofocus autocomplete>
                                </div>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-static mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input id="register-form-password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        name="password" placeholder="{{ __('Password') }}" type="password"
                                        value="{{ old('password') }}" required autocomplete>
                                </div>
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-static mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input id="register-form-password-confirmation"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        name="password_confirmation" placeholder="{{ __('Confirm Password') }}"
                                        type="password" value="{{ old('password') }}" required
                                        autocomplete="new-password">
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-static mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input id="register-form-phone"
                                        class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                        name="phone" placeholder="{{ __('Phone') }}" type="phone"
                                        value="{{ old('phone') }}" required autocomplete>
                                </div>
                                @if ($errors->has('phone'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                @endif
                            </div>
                            {{-- @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                            <div class="form-check text-start">
                                <input class="form-check-input bg-dark border-dark" type="checkbox" value=""
                                    id="flexCheckDefault" checked>
                                <label class="form-check-label" for="flexCheckDefault">
                                    I agree the <a href=" {{route('terms.show')}} "
                                        class="text-dark font-weight-bolder">Terms and
                                        Conditions</a>
                                </label>
                            </div>
                            @endif --}}
                            <div class="text-center">
                                <button type="button" id="register-button"
                                    class="btn bg-gradient-success w-100 my-4 mb-2">Sign
                                    up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






@endsection

@section('page-js')
<script src="/js/signup.js"></script>
@endsection
