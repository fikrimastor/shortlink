@extends('layouts.public',[
'activePage' => 'signup',
'parentPage' => '',
'menuParent' => '',
'title' => __('Signup')
])
@section('content')
<div class="page-header position-relative">
    <div class="container py-6 postion-relative z-index-2">
        <div class="row">
            <div class="col-md-6 mx-auto text-center">
                <img src="/logo-veegro.png" class="my-4" style="width: 150px" alt="Veegro">
                <h2 class="text-dark">Review Your Details</h2>
            </div>
        </div>
        <div class="row justify-content-evenly mt-2">
            <div class="col-sm-5 ">

                <div class="card bg-gray-600">
                    <div class="card-body">
                        <h5 class="text-white mb-2">{{__('Your Details:')}}</h5>
                        <p class="text-white"> Name: {{Auth::user()->name}} </p>
                        <p class="text-white"> Email: {{Auth::user()->email}}</p>
                        <p class="text-white"> Phone: {{Auth::user()->profile->phone}}</p>
                        <p class="text-white"> Plans: {{$plan->name}}</p>

                    </div>
                </div>
            </div>
            <div class="col-sm-5">

                <div class="card bg-gradient-gray">
                    <div class="card-body">
                        <h5 class="text-dark mb-2"> {{__('Payment Method:')}} </h5>
                        <div class="form-check">
                            <input type="radio" name="radiogroup" id="radio-securepay" class="radiogroup form-check-input"
                                value="Pay with SecurePay" data-value="{{$plan->id}}" data-url="/signup/go/securepay" checked>
                            <label class="form-check-label" for="radioSecurepay">
                                Securepay
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="radiogroup form-check-input" type="radio" name="radiogroup" value="Pay with Billplz"
                                data-value="{{$plan->id}}" data-url="/signup/go/billplz">
                            <label class="form-check-label" for="radioBillplz">
                                Billplz
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="radiogroup form-check-input" type="radio" name="radiogroup" value="Pay with Bizappay"
                                data-value="{{$plan->id}}" data-url="/signup/go/bizappay">
                            <label class="form-check-label" for="radioBizappay">
                                Bizappay
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="radiogroup form-check-input" type="radio" name="radiogroup" value="Pay with Toyyibpay"
                                data-value="{{$plan->id}}" data-url="/signup/go/toyyibpay" disabled>
                            <label class="form-check-label" for="radioToyyibpay">
                                Toyyibpay
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="radiogroup form-check-input" type="radio" name="radiogroup" value="Pay with Stripe"
                                data-value="{{$plan->id}}" data-url="/signup/go/stripe" disabled>
                            <label class="form-check-label" for="radioStripe">
                                Stripe
                            </label>
                        </div>
                        <form id="action" action="{{ route('signup.go', 'securepay') }}" method="post">
                            @csrf
                            <input id="value" type="hidden" name="plan_id" value="{{$plan->id}}">
                            <button id="url" type="submit" href="" class="btn btn-primary">
                                {{__('Pay Now')}}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid px-5 my-6 bottom-10">
    <div class="mt-n8">
        <div class="container">
            <div class="tab-content tab-space">
                <div class="tab-pane active" id="price">
                    <div class="row">

                        {{-- <div class="col-lg-4 mb-lg-0 mb-4">
                            <div class="card shadow-lg">
                                <span class="badge rounded-pill bg-light text-dark w-30 mt-n2 mx-auto">Enterprise</span>
                                <div class="card-header text-center pt-4 pb-3">
                                    <h1 class="font-weight-bold mt-2">
                                        <small class="text-lg align-top me-1">$</small>99<small
                                            class="text-lg">/mo</small>
                                    </h1>
                                </div>
                                <div class="card-body text-lg-start text-center pt-0">
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">Unlimited team members</span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">100GB Cloud storage </span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">Integration help </span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">Sketch Files </span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">API Access </span>
                                    </div>
                                    <div class="d-flex justify-content-lg-start justify-content-center p-2">
                                        <i class="material-icons my-auto">done</i>
                                        <span class="ps-3">Complete documentation </span>
                                    </div>
                                    <a href="javascript:;" class="btn btn-icon bg-gradient-dark d-lg-block mt-3 mb-0">
                                        Join
                                        <i class="fas fa-arrow-right ms-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-js')
<script src="/js/payment-gateway.js"></script>
@endsection
