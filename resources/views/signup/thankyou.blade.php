@extends('layouts.public',[
'activePage' => 'signup',
'parentPage' => '',
'menuParent' => '',
'title' => __('Signup')
])


@section('content')

<div class="page-header position-relative">
    <div class="container py-6 postion-relative z-index-2">
        <div class="row">
            <div class="col-md-6 mx-auto text-center">
                <img src="/logo-veegro.png" class="my-4" style="width: 150px" alt="Veegro">
                <h2 class="text-dark">{{__('Thank You!')}}</h2>
            </div>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col-sm-5">
                <div class="card">
                    <div class="card-body">
                        @if ($verified)
                        <p>Terima kasih di atas pembayaran. Sila login untuk melihat tarikh luput baharu.</p>
                        
                        @else
                        <p>Pembayaran anda akan diproses. Sila login untuk menyemak status keahlian.</p>
                        
                        @endif
                        <a href="/login" class="btn btn-primary">Login</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


@endsection
