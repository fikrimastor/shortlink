@extends('layouts.private',[
'activePage' => 'teamIndex',
'parentPage' => 'Team',
'menuParent' => '',
'title' => __('Teams Settings')
])

@section('content')
<div class="container-fluid my-3 py-3">
    @if( session('success') )
    @include('layouts.alerts.feedback')
    @endif
    <div class="row mb-5">
        <div class="col-lg-3">
            <div class="card position-sticky top-1">
                <ul class="nav flex-column bg-white border-radius-lg p-3">
                    <li class="nav-item">
                        <a class="nav-link text-dark d-flex" data-scroll="" href="#teamSettings">
                            <i class="material-icons text-lg me-2">settings_suggest</i>
                            <span class="text-sm">{{__('Team Settings')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark d-flex" data-scroll="" href="#teamMemberList">
                            <i class="material-icons text-lg me-2">groups</i>
                            <span class="text-sm">{{__('List Of Team Members')}}</span>
                        </a>
                    </li>
                    <li class="nav-item pt-2">
                        <a class="nav-link text-dark d-flex" data-scroll="" href="#teamInvitation">
                            <i class="material-icons text-lg me-2">group_add</i>
                            <span class="text-sm">{{__('Invite To Team')}}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-9 mt-lg-0 mt-4">
            <!-- Card teamSettings -->
            <div class="card" id="teamSettings">
                <div class="card-body">
                    {{-- <div class="card-header py-3">
                    </div> --}}
                    <h6 class="m-0 font-weight-bold text-primary">{{__('Team Settings')}}</h6>
                    <p>{{ __('You may edit team profiles here.') }}</p>
                    <form class="form-horizontal" method="post" action="{{route('teams.update', $team->uuid)}}">
                        @method('PUT')
                        @csrf
                        <div class="form-group @error('name') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="teamName">{{__('Team Name')}}</label>
                                <br />
                                <input type="text" class="form-control" id="teamName" name="name"
                                    placeholder="{{ old('name', $team->name) }}" value="{{ old('name', $team->name) }}">
                                @error('name')
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group mt-4 @error('description') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="teamDescription">{{__('Team Description')}}</label>
                                <br />
                                <input type="text" class="form-control" id="teamDescription" name="description"
                                    placeholder="{{ old('description', $team->profile->description) }}"
                                    value="{{ old('description', $team->profile->description) }}">
                                @error('description')
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mt-4 @error('city') has-error text-danger @enderror">
                            <div class="input-group input-group-static">
                                <label for="teamLocation">{{__('Team Location')}}</label>
                                <br />
                                <input type="text" class="form-control" id="teamLocation" name="city"
                                    placeholder="{{ old('city', $team->profile->city) }}"
                                    value="{{ old('city', $team->profile->city) }}">
                                @error('city')
                                <span class="help-block">
                                    <strong>{{ $errors->first('city') }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="d-flex justify-content-end mt-4">
                            <a href="{{route('teams.index', $team->uuid)}}"
                                class="btn btn-light m-0">{{__('Cancel')}}</a>
                            <button type="Submit" name="button" class="btn bg-gradient-success m-0 ms-2"><i
                                    class="fa fa-btn fa-save"></i> {{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Card teamMemberList -->
            <div class="card mt-4" id="teamMemberList">
                <div class="card-body">
                    {{-- <div class="card-header py-3">
                    </div> --}}
                    <h6 class="m-0 font-weight-bold text-primary">{{__('Team Members')}}</h6>
                    <p>{{ __('All of the people that are part of this team.') }}</p>
                    <table class="table table-stripe">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">NAMA</th>
                                <th class="text-center">ROLE</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($team->users->sortBy('name') as $user)
                            <tr>
                                <td class="text-center">
                                    <div class="pr-3">
                                        <img width="32" class="rounded-circle" src="{{ $user->profile_photo_url }}">
                                    </div>
                                    <span>{{ $user->name }}</span>
                                </td>
                                <td class="text-center">{{$user->name}}</td>
                                <td class="text-center">@foreach ($user->roles()->pluck('name') as $role)
                                    <span class="badge badge-info">{{ $role }}</span><br />
                                    @endforeach
                                </td>
                                <td class="text-center">
                                    <!--- Modals Team Role edit --->
                                    {{-- <button type="button" class="btn btn-secondary" data-bs-toggle="modal"
                                        data-bs-target="#editRoleMember">
                                        Launch demo modal
                                    </button> --}}
                                    @if (Gate::check('addTeamMember', $team) && Laravel\Jetstream\Jetstream::hasRoles())
                                    <button class="btn btn-link text-secondary"
                                        wire:click="manageRole({{ $user->id }})">
                                        {{ Laravel\Jetstream\Jetstream::findRole($user->membership->role)->name }}
                                    </button>
                                    @elseif (Laravel\Jetstream\Jetstream::hasRoles())
                                    <button class="btn btn-link text-secondary disabled text-decoration-none ms-2">
                                        {{ Laravel\Jetstream\Jetstream::findRole($user->membership->role)->name }}
                                    </button>
                                    @endif
                                    {{-- <a href="{{ route('admin.users.edit', $user->id) }}"
                                        class="btn btn-sm btn-secondary">EDIT</a> --}}

                                    {{-- <form method="post" class="d-inline"
                                        action="{{ route('admin.users.destroy', $user->id) }}" onsubmit="return confirm('Are you sure you want to delete {{$user->name}}
                                    ({{$user->id}})?');">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">DELETE</button>
                                    </form> --}}
                                    <!-- Leave Team -->
                                    @if ($this->user->id === $user->id)
                                    <button class="btn btn-link text-danger text-decoration-none"
                                        wire:click="$toggle('confirmingLeavingTeam')">
                                        {{ __('Leave') }}
                                    </button>
                                    <!-- Remove Team Member -->
                                    @elseif (Gate::check('removeTeamMember', $team))
                                    <button class="btn btn-link text-danger text-decoration-none"
                                        wire:click="confirmTeamMemberRemoval('{{ $user->id }}')">
                                        <div wire:loading wire:target="confirmTeamMemberRemoval"
                                            class="spinner-border spinner-border-sm" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>

                                        {{ __('Remove') }}
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Card Team Member List -->
            <div class="card mt-4" id="teamInvitation">
                {{-- <div class="card-header py-3">
                </div> --}}
                <div class="card-body">
                    <h6 class="m-0 font-weight-bold text-primary">{{__('Pending Approve Membersips')}}</h6>
                    <p>{{ __('These people have been invited to your team and have been sent an invitation email. They
                        may join the team by accepting
                        the email invitation.') }}</p>
                    <table class="table table-stripe">
                        <thead>
                            <tr>
                                <th class="text-center">DETAIL</th>
                                <th class="text-center">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($team->teamInvitations as $invitation)
                            <tr>
                                <td class="text-center">
                                    {{ $invitation->email }}
                                </td>
                                <td class="text-center">{{$user->name}}</td>
                                <td class="text-center">@foreach ($user->roles()->pluck('name') as $role)
                                    <span class="badge badge-info">{{ $role }}</span><br />
                                    @endforeach
                                </td>
                                <td class="text-center">
                                    @if (Gate::check('removeTeamMember', $team))
                                    <!-- Cancel Team Invitation -->
                                    <button class="btn btn-link text-danger text-decoration-none"
                                        wire:click="cancelTeamInvitation({{ $invitation->id }})">
                                        <div wire:loading wire:target="cancelTeamInvitation"
                                            class="spinner-border spinner-border-sm" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>

                                        {{ __('Cancel') }}
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footers.auth')
</div>
<!-- Modal -->
<div class="modal fade" id="editRoleMember" tabindex="-1" aria-labelledby="editRoleMember" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- Leave Team Confirmation Modal -->
{{-- <x-jet-confirmation-modal wire:model="confirmingLeavingTeam">
    <x-slot name="title">
        {{ __('Leave Team') }}
    </x-slot>

    <x-slot name="content">
        {{ __('Are you sure you would like to leave this team?') }}
    </x-slot>

    <x-slot name="footer">
        <x-jet-secondary-button wire:click="$toggle('confirmingLeavingTeam')" wire:loading.attr="disabled">
            {{ __('Cancel') }}
        </x-jet-secondary-button>

        <x-jet-danger-button class="ms-2" wire:click="leaveTeam" wire:loading.attr="disabled">
            {{ __('Leave') }}
        </x-jet-danger-button>
    </x-slot>
</x-jet-confirmation-modal>

<!-- Remove Team Member Confirmation Modal -->
<x-jet-confirmation-modal wire:model="confirmingTeamMemberRemoval">
    <x-slot name="title">
        {{ __('Remove Team Member') }}
    </x-slot>

    <x-slot name="content">
        {{ __('Are you sure you would like to remove this person from the team?') }}
    </x-slot>

    <x-slot name="footer">
        <x-jet-secondary-button wire:click="$toggle('confirmingTeamMemberRemoval')" wire:loading.attr="disabled">
            {{ __('Cancel') }}
        </x-jet-secondary-button>

        <x-jet-danger-button class="ms-2" wire:click="removeTeamMember" wire:loading.attr="disabled">
            {{ __('Remove') }}
        </x-jet-danger-button>
    </x-slot>
</x-jet-confirmation-modal> --}}
@endsection
