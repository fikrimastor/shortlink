@extends('layouts.private',[
'activePage' => 'teamMembers',
'parentPage' => 'Team',
'menuParent' => 'teams.index',
'title' => __('Teams Members')
])

@section('content')
<div class="container-fluid py-4">
    <section class="py-3">
        @if( session('success') )
        @include('layouts.alerts.feedback')
        @endif
        <div class="row mb-4 mb-md-0">
            <div class="col-md-8 me-auto my-auto text-left">
                <h5>{{__('Members of team')}} "{{$team->name}}"</h5>
                <p>You can see all your teams here. <br> You may edit, view or switch team from this page.</p>

            </div>
            <div class="col-sm-3 ">
                <div class="row justify-content-between">
                    <div class="col-4">
                        <a href="{{route('teams.create')}}" class="btn bg-gradient-secondary mb-3 btn-tooltip" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{__('New Team')}}" data-container="body" data-animation="true">
                            <i class="material-icons text-white">add</i>
                        </a>
                    </div>
                    <div class="col-4">
                        <a href="{{route('teams.edit', $team)}}" class="btn bg-gradient-warning mb-3 btn-tooltip" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{__('Edit Team')}}" data-container="body" data-animation="true">
                            <i class="material-icons text-white">tune</i>
                        </a>
                    </div>
                    <div class="col-4">
                        <button type="button" class="btn btn-block bg-gradient-success mb-3 btn-tooltip" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{__('Invite To Team')}}" data-container="body" data-animation="true" data-bs-toggle="modal"
                            data-bs-target="#modal-team-invite"><i
                                class="material-icons text-white">group_add</i></button>
                        <div class="modal fade" id="modal-team-invite" tabindex="-1" role="dialog"
                            aria-labelledby="modal-team-invite" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h6 class="modal-title" id="modal-title-default">
                                            {{__('Invite to team')}} {{$team->name}}</h6>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card card-plain">
                                            <div class="card-body">
                                                <form method="post" action="{{route('teams.members.invite', $team)}}"
                                                    role="form text-left" style="text-align: left;">
                                                    @csrf
                                                    <div class="form-group @error('email') has-error @enderror">

                                                        {{-- <label class="text-left">{{__('E-Mail Address')}}</label>
                                                        <div
                                                            class="input-group input-group input-group-dynamic mb-3 text-left">
                                                            <input type="email" class="form-control" name="email"
                                                                aria-label="Email" aria-describedby="email-addon">
                                                            @error('email')
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div> --}}
                                                        <label class="text-left">{{__('Insert Member E-Mail
                                                            Address')}}</label><br />
                                                        <div class="input-group input-group-static mb-4">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text text-left"><i
                                                                        class="ni ni-email-83"></i></span>
                                                            </div>
                                                            <input
                                                                class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                                placeholder="{{ __('Email') }}" type="email"
                                                                name="email" aria-label="Email"
                                                                aria-describedby="email-addon" required autofocus>
                                                            @error('email')
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="text-center">
                                                        <button type="submit" name="button"
                                                            class="btn btn-round bg-gradient-info btn-lg w-100 mt-4 mb-0">{{__('Invite
                                                            to team')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-12 mt-4 mt-lg-0">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <h5 class="mb-0">{{__('Existing Member')}}</h5>
                            <p class="text-sm mb-0">
                                {{__('The list of existing member.')}}
                            </p>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-xxs font-weight-bolder opacity-7">
                                            {{__('Name')}}</th>
                                        <th class="text-uppercase text-xxs font-weight-bolder opacity-7 ps-2">
                                            {{__('Action')}}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($team->users as $user)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <h6 class="mb-0 font-weight-normal text-sm">{{$user->name}}</h6>
                                            </div>
                                        </td>
                                        <td>
                                            @if(auth()->user()->isOwnerOfTeam($team))
                                            @if(auth()->user()->getKey() !== $user->getKey())
                                            <form style="display: inline-block;"
                                                action="{{route('teams.members.destroy', [$team, $user])}}"
                                                method="post">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button class="btn btn-danger btn-sm"><span
                                                        class="material-icons text-white position-relative text-md pe-2">
                                                        delete
                                                    </span>
                                                    Delete</button>
                                            </form>
                                            @endif
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mt-4 mt-lg-0">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <h5 class="mb-0">{{__('Pending invitations')}}</h5>
                            <p class="text-sm mb-0">
                                {{__('The list of pending accept invitation user.')}}
                            </p>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-xxs font-weight-bolder opacity-7">
                                            {{__('E-Mail')}}
                                        </th>
                                        <th class="text-uppercase text-xxs font-weight-bolder opacity-7 ps-2">
                                            {{__('Action')}}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($team->invites as $invite)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <h6 class="mb-0 font-weight-normal text-sm">{{$invite->email}}</h6>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{route('teams.members.resend_invite', $invite)}}"
                                                class="btn btn-sm btn-warning">
                                                <span class="material-icons text-white position-relative text-md pe-2">
                                                    forward_to_inbox
                                                </span>{{__('Resend invite')}}
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    {{-- @include('layouts.footers.auth') --}}
</div>
@endsection
