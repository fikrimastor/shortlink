@extends('layouts.private',[
'activePage' => 'teamRole',
'parentPage' => 'Team',
'menuParent' => 'teams.index',
'title' => __('Teams Roles Settings')
])

@section('content')
<div class="container-fluid py-4">
    <section class="py-3">
        @if( session('success') )
        @include('layouts.alerts.feedback')
        @endif
        <div class="row mb-4 mb-md-0">
            <div class="col-md-8 me-auto my-auto text-left">
                <h5>{{__('Team roles for')}} {{$team->name}}</h5>
                <p>You can see all your teams roles here. <br> You may create, edit or delete team roles from this page.</p>

            </div>
            <div class="col-sm-3">
                <div class="row justify-content-between">
                    <div class="col-4">
                        <a href="{{route('teams.roles.create')}}" class="btn bg-gradient-secondary mb-3 btn-tooltip" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{__('New Team')}}" data-container="body" data-animation="true">
                            <i class="material-icons text-white">add</i>
                        </a>
                    </div>
                    <div class="col-4">
                        <a href="{{route('teams.roles.edit', $team)}}" class="btn bg-gradient-warning mb-3 btn-tooltip" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{__('Edit Team')}}" data-container="body" data-animation="true">
                            <i class="material-icons text-white">tune</i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12 mt-4 mt-lg-0">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <h5 class="mb-0">{{__('Existing Member')}}</h5>
                            <p class="text-sm mb-0">
                                {{__('The list of existing member.')}}
                            </p>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-xxs font-weight-bolder opacity-7">
                                            {{__('Name')}}</th>
                                        <th class="text-uppercase text-xxs font-weight-bolder opacity-7 ps-2">
                                            {{__('Permission')}}
                                        </th>
                                        <th class="text-uppercase text-xxs font-weight-bolder opacity-7 ps-2">
                                            {{__('Action')}}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($team->users as $user)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <h6 class="mb-0 font-weight-normal text-sm">{{$user->name}}</h6>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex px-2 py-1">
                                                <h6 class="mb-0 font-weight-normal text-sm">{{$user->name}}</h6>
                                            </div>
                                        </td>
                                        <td>
                                            @if(auth()->user()->isOwnerOfTeam($team))
                                            @if(auth()->user()->getKey() !== $user->getKey())
                                            <form style="display: inline-block;"
                                                action="{{route('teams.members.destroy', [$team, $user])}}"
                                                method="post">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button class="btn btn-danger btn-sm"><span
                                                        class="material-icons text-white position-relative text-md pe-2">
                                                        delete
                                                    </span>
                                                    Delete</button>
                                            </form>
                                            @endif
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <div class="row">
                
            </div>
    </section>
    {{-- @include('layouts.footers.auth') --}}
</div>
@endsection
