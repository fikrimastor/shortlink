@extends('layouts.public',[
'activePage' => 'welcome',
'parentPage' => '',
'menuParent' => '',
'title' => __('Welcome')
])
@section('content')

<div class="page-header align-items-start min-vh-100"
    style="background-image: url('/material/assets/img/sheeps-register.jpg');">
    <span class="mask bg-gradient-dark opacity-6"></span>
    <div class="container my-auto">
        <div class="row mb-8">
            <div
                class="col-lg-6 col-md-7 d-flex justify-content-center text-md-start text-center flex-column mt-sm-0 mt-7">
                <img src="/logo-veegro.png" class="my-4" style="width: 271px" alt="Veegro">
                <h1 class="text-white">Your Perfect Place for Work</h1>
                <p class="lead pe-md-5 me-md-5 text-white opacity-8">Manage your farm and breeding programme in one
                    place.
                    It's better to start now or never.
                </p>
                <div class="buttons">
                    <a href="{{route('signup')}}" class="btn bg-gradient-success mt-4">Register</a>
                    <a href="{{route('login')}}" class="btn btn-outline-success text-white mt-4">Login</a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
