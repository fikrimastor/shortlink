<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\SignUpController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Teamwork\AuthController;
use App\Http\Controllers\Teamwork\TeamController;
use App\Http\Controllers\Admin\AdminPlanController;
use App\Http\Controllers\Teamwork\TeamRoleController;
use App\Http\Controllers\Admin\AdminPaymentController;
use App\Http\Controllers\Teamwork\TeamMemberController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\RoleController as AdminRoleController;
use App\Http\Controllers\Admin\PermissionController as AdminPermissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('dashboard', 'dashboard')
	->name('dashboard')
	->middleware(['auth', 'verified']);

// Route::get('/profile/user', function () {
//     return view('profile.show');
// })->name('profile.show');

// Route::resource('user', ProfileController::class);

Route::get('/signup', [SignUpController::class, 'index'])->name('signup');
Route::group(['middleware' => ['auth'],'prefix' => 'signup'],function () {

    Route::get('/review/{id}', [SignUpController::class, 'review'])->name('signup.review');
    Route::get('/pay/{payment_link}', [SignUpController::class, 'pay'])->name('signup.pay');
    Route::post('/go/{payment_gateway}', [SignUpController::class, 'go'])->name('signup.go');
    Route::post('/thankyou/{payment_gateway}', [SignUpController::class, 'thankyou'])->name('signup.thankyou');
    Route::get('/thankyou/{payment_gateway}', [SignUpController::class, 'thankyou']);
    Route::get('/callback/{payment_gateway}', [SignUpController::class, 'callback'])->name('signup.callback');
    Route::post('/callback/{payment_gateway}', [SignUpController::class, 'callback']);


});

// Regular users
Route::group(['middleware' => ['auth'],'prefix' => 'users'], function () {

    Route::get('/profile/{user}', [ProfileController::class, 'show'])->name('user-profile.show');
    Route::put('/profile/{user}/edit', [ProfileController::class, 'update'])->name('user-profile.update');

    Route::resource('plans', PlanController::class);
    Route::resource('payment', PaymentController::class);
});


/**
 * Admin routes
 */
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' =>'admin.'], function () {

    Route::resource('plans', AdminPlanController::class);
    Route::resource('users', AdminUserController::class);

    Route::resource('roles', AdminRoleController::class);
    Route::resource('permissions', AdminPermissionController::class);
});


/**
 * Teamwork Roles & Permission routes
 */
Route::group(['middleware' => ['auth'], 'prefix' => 'teams', 'as' => 'teams.'], function ()
{
    Route::resource('roles', TeamRoleController::class);
    Route::resource('permissions', TeamRoleController::class);
});

/**
 * Teamwork routes
 */
Route::group(['middleware' => ['auth'], 'prefix' => 'teams', 'namespace' => 'Teamwork'], function()
{
    Route::get('/', [TeamController::class, 'index'])->name('teams.index');
    Route::get('create', [TeamController::class, 'create'])->name('teams.create');
    Route::get('/{id}', [TeamController::class, 'show'])->name('teams.show')->can('update');
    Route::post('teams', [TeamController::class, 'store'])->name('teams.store');
    Route::get('edit/{id}', [TeamController::class, 'edit'])->name('teams.edit')->can('update');
    Route::put('edit/{id}', [TeamController::class, 'update'])->name('teams.update')->can('update');
    Route::delete('destroy/{id}', [TeamController::class, 'destroy'])->name('teams.destroy')->can('delete');
    Route::get('switch/{id}', [TeamController::class, 'switchTeam'])->name('teams.switch');

    Route::get('members/{id}', [TeamMemberController::class, 'show'])->name('teams.members.show');
    Route::get('members/resend/{invite_id}', [TeamMemberController::class, 'resendInvite'])->name('teams.members.resend_invite');
    Route::post('members/{id}', [TeamMemberController::class, 'invite'])->name('teams.members.invite');
    Route::delete('members/{id}/{user_id}', [TeamMemberController::class, 'destroy'])->name('teams.members.destroy');

    Route::get('accept/{token}', [AuthController::class, 'acceptInvite'])->name('teams.accept_invite');
});
